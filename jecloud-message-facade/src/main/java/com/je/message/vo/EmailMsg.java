/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.message.vo;

import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.google.common.base.Strings;
import com.je.common.base.constants.message.SendContextType;

import java.util.ArrayList;
import java.util.List;

public class EmailMsg {
    /**
     * 接受邮箱
     */
    private String receiveEmail;
    /**
     * 密送
     */
    private String ms;
    /**
     * 抄送
     */
    private String cs;
    /**
     * 分别发送
     */
    private String fb;
    /**
     * 主题
     */
    private String subject;
    /**
     * 内容类型   文本与HTML
     */
    private String contextType = SendContextType.HTML;
    /**
     * 内容
     */
    private String context;
    /**
     * 紧急状态     1.紧急     2.普通   3.缓慢
     */
    private String faster;
    /**
     * 需要回执   1 回执    0 不回执
     */
    private String replySign;
    /**
     * 文件名称
     */
    private List<String> fileNames = new ArrayList<String>();
    /**
     * 文件地址
     */
    private List<String> addresses = new ArrayList<String>();
    /**
     * 分类内容
     */
    private String fl;
    /**
     * 公司ID
     */
    private String jtgsId;

    public EmailMsg() {

    }

    /**
     * 不带附件的邮件
     *
     * @param receiveEmail
     * @param subject
     * @param contextType
     * @param context
     */
    public EmailMsg(String receiveEmail, String subject, String contextType,
                    String context) {
        super();
        this.receiveEmail = receiveEmail;
        this.subject = subject;
        this.contextType = contextType;
        this.context = context;
    }

    /**
     * 带有文件的邮件
     *
     * @param receiveEmail
     * @param subject
     * @param contextType
     * @param context
     * @param fileNames
     * @param addresses
     */
    public EmailMsg(String receiveEmail, String subject, String contextType,
                    String context, List<String> fileNames, List<String> addresses) {
        super();
        this.receiveEmail = receiveEmail;
        this.subject = subject;
        this.contextType = contextType;
        this.context = context;
        this.fileNames = fileNames;
        this.addresses = addresses;
    }


    public String getReceiveEmail() {
        return receiveEmail;
    }

    public String getMs() {
        return ms;
    }

    public String getCs() {
        return cs;
    }

    public String getFb() {
        return fb;
    }

    public String getSubject() {
        return subject;
    }

    public String getContextType() {
        return contextType;
    }

    public String getContext() {
        return context;
    }

    public String getFaster() {
        return faster;
    }

    public String getReplySign() {
        return replySign;
    }

    public List<String> getFileNames() {
        return fileNames;
    }

    public List<String> getAddresses() {
        return addresses;
    }

    public String getFl() {
        return fl;
    }

    public String getJtgsId() {
        return jtgsId;
    }


    public void setReceiveEmail(String receiveEmail) {
        this.receiveEmail = receiveEmail;
    }

    public void setMs(String ms) {
        this.ms = ms;
    }

    public void setCs(String cs) {
        this.cs = cs;
    }

    public void setFb(String fb) {
        this.fb = fb;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public void setContextType(String contextType) {
        this.contextType = contextType;
    }

    public void setContext(String context) {
        this.context = context;
    }

    public void setFaster(String faster) {
        this.faster = faster;
    }

    public void setReplySign(String replySign) {
        this.replySign = replySign;
    }

    public void setFileNames(List<String> fileNames) {
        this.fileNames = fileNames;
    }

    public void setAddresses(List<String> addresses) {
        this.addresses = addresses;
    }

    public void setFl(String fl) {
        this.fl = fl;
    }

    public void setJtgsId(String jtgsId) {
        this.jtgsId = jtgsId;
    }


    public EmailMsg receiveEmail(String receiveEmail) {
        this.receiveEmail = receiveEmail;
        return this;
    }

    public EmailMsg ms(String ms) {
        this.ms = ms;
        return this;
    }

    public EmailMsg cs(String cs) {
        this.cs = cs;
        return this;
    }

    public EmailMsg fb(String fb) {
        this.fb = fb;
        return this;
    }

    public EmailMsg subject(String subject) {
        this.subject = subject;
        return this;
    }

    public EmailMsg contextType(String contextType) {
        this.contextType = contextType;
        return this;
    }

    public EmailMsg context(String context) {
        this.context = context;
        return this;
    }

    public EmailMsg faster(String faster) {
        if (Strings.isNullOrEmpty(faster)) {
            faster = "2";
        }
        this.faster = faster;
        return this;
    }

    public EmailMsg replySign(String replySign) {
        if (Strings.isNullOrEmpty(replySign)) {
            replySign = "0";
        }
        this.replySign = replySign;
        return this;
    }

    public EmailMsg files(List<String> fileNames,List<String> addresses) {
        this.fileNames = fileNames;
        this.addresses = addresses;
        return this;
    }

    public EmailMsg fl(String fl) {
        this.fl = fl;
        return this;
    }

    public EmailMsg jtgsId(String jtgsId) {
        this.jtgsId = jtgsId;
        return this;
    }

    public static EmailMsg build() {
        return new EmailMsg();
    }


}
