FROM registry.cn-beijing.aliyuncs.com/kaite-open/jdk:8.4
LABEL maintainer="hoseazhai@163.com"

add aggregate-tomcat/target/jeapp.jar /data/jecloud/
RUN ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime \
    && echo "Asia/Shanghai" > /etc/timezone
ENTRYPOINT ["java","-Dapollo.meta=http://jecloud-apollo:8080","-Dservicecomb.service.registry.instance.healthCheck.interval=10","-Dservicecomb.uploads.directory=/data/jecloud/tmp","-Dservicecomb.downloads.directory=/data/jecloud/tmp","-Dlog.file.path=/data/jecloud/logs","-Dservicecomb.service.registry.instance.preferIpAddress=true","-Dservicecomb.service.registry.address=http://jecloud-comb:30100","-Dservicecomb.datacenter.region=test","-Dservice_description.environment=development"]
