/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.message.rpc;


import com.je.common.base.message.vo.*;

import java.util.List;

public interface SocketPushMessageRpcService {

    /**
     * 发送消息统一接口
     *
     * @param message
     */
    void sendMessage(PushMessage message);

    /**
     * 发送系统消息
     *
     * @param message
     */
    void sendSystemMessage(PushSystemMessage message);

    /**
     * 发送脚本消息
     *
     * @param message
     */
    void sendScriptMessage(PushScriptMessage message);

    /**
     * 发送脚本消息
     */
    void sendScriptOpenFuncGridMsgToUser(String userId,String funcCode,String busType, String content);

    /**
     * 发送脚本消息
     */
    void sendScriptOpenFuncGridMsgToUsers(List<String> userIdList, String funcCode, String busType, String content);

    /**
     * 发送脚本消息
     */
    void sendScriptOpenFuncFormMsgToUser(String userId, String funcCode,String busType, String content,String beanId);

    /**
     * 发送脚本消息
     */
    void sendScriptOpenFuncFormMsgToUsers(List<String> userIdList, String funcCode,String busType, String content,String beanId);

    /**
     * 发送未读消息
     *
     * @param message
     */
    void sendNoReadMessage(PushNoReadMessage message);

    /**
     * 发送通知消息
     *
     * @param message
     */
    void sendNoticeMessage(PushNoticeMessage message);

    /**
     * 发送脚本消息
     */
    void sendNoticeOpenFuncGridMsgToUser(String userId, String funcCode,String busType, Notice content);

    /**
     * 发送脚本消息
     */
    void sendNoticeOpenFuncGridMsgToUsers(List<String> userIdList, String funcCode,String busType, Notice content);

    /**
     * 发送脚本消息
     */
    void sendNoticeOpenFuncFormMsgToUser(String userId, String funcCode,String busType, Notice content,String beanId);

    /**
     * 发送脚本消息
     */
    void sendNoticeOpenFuncFormMsgToUsers(List<String> userIdList, String funcCode,String busType, Notice content,String beanId);
}
