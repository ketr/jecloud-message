/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.message.vo;

import java.util.HashMap;

public enum CodeEnum {

    //响应码枚举
    SUCCESS(200,"验证码发送成功"),
    FALSE(9999,"发送失败，请联系管理员！"),
    _9000(9000,"手机号为空"),

    _9001(9001,"每日可发送验证码超过次数"),

    _9002(9002,"验证码超过时效时长");

    /**
     * 响应码
     */
    private int code;

    /**
     * 响应码对应消息
     */
    private String msg;

    CodeEnum(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

    @Override
    public String toString() {
        return code + ":" + msg;
    }

    private static final HashMap<Integer, String> map = new HashMap<>();

    static {
        map.put(CodeEnum.SUCCESS.getCode(), CodeEnum.SUCCESS.getMsg());
        map.put(CodeEnum._9000.getCode(), CodeEnum._9000.getMsg());
        map.put(CodeEnum._9001.getCode(), CodeEnum._9001.getMsg());
        map.put(CodeEnum._9002.getCode(), CodeEnum._9002.getMsg());
    }

    public static String getMessage(Integer i) {
        return map.get(i);
    }
}
