/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.message.rpc;

import com.je.message.Push;
import com.je.message.vo.Message;

import java.util.List;

/**
 * 消息推送
 *
 * @author wangmm@ketr.com.cn
 * @date 2020/5/7
 */
public interface PlatformGeneralPushRpcService {

    /**
     * 自定义消息发送(使用默认配置发送方式)
     *
     * @param message 消息
     * @return int 成功数量
     */
    default int sendMessage(Message message) {
        return sendPlatformMessage(null, message);
    }

    /**
     * 根据业务模板发送消息(使用默认配置发送方式)
     *
     * @param message  消息
     * @param category 业务类别编码
     * @return int 成功数量
     */
    default int sendCategoryMessage(Message message, String category) {
        return sendMessageWithPlatformAndCategory(null, message, category);
    }

    /**
     * 自定义消息发送
     *
     * @param platforms 需要发送的平台
     * @param message   消息
     * @return int 成功数量
     */
    int sendPlatformMessage(List<Push> platforms, Message message);

    /**
     * 根据业务模板发送消息
     *
     * @param platforms 需要发送的平台
     * @param message   消息
     * @param category  业务类别编码
     * @return int 成功数量
     */
    int sendMessageWithPlatformAndCategory(List<Push> platforms, Message message, String category);
}
