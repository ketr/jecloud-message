package com.je.message.service.wework;

import com.je.message.vo.weWork.AbstractWeWorkNotice;

public interface WeWorkService {

    void send(AbstractWeWorkNotice abstractWeWorkNotice);

}
