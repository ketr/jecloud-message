/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.message.vo;

import com.alibaba.fastjson2.JSONObject;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Message
 *
 * @author wangmm@ketr.com.cn
 * @date 2020/5/7
 */
public class Message implements Serializable {

    private static final long serialVersionUID = -1L;

    // 基础参数
    /**
     * 用户ID
     */
    private String userId;
    /**
     * 用户ID
     */
    private String deptId;
    /**
     * 推送组ID
     */
    private String groupId;
    /**
     * 标题
     */
    private String title;
    /**
     * 内容
     */
    private String content;
    /**
     * 按钮文本
     */
    private String buttonText;
    /**
     * 按钮事件或url
     */
    private String buttonAction;
    /**
     * 业务数据
     */
    private Map<String, Object> bean = new HashMap<>();
    /**
     * 拓展参数
     */
    private JSONObject params = new JSONObject();

    // DwrMsgVo 特殊参数
    /**
     * 验证用户是否登录操作   用于是否发送html5提醒
     */
    private Boolean login = false;
    /**
     * 是否没有登录，下次登录调用
     */
    private Boolean loginHistory = false;

    // 个推特殊参数，参考 AppSendTread
    /**
     * 0|1
     */
    private String onlySocket = "0";
    /**
     * PayloadInfo
     */
    private PayloadInfo payloadInfo;

    /**
     * WEB推送特殊参数 是否添加内容到通通知列表
     */
    private boolean addMsgList;

    /**
     * WEB推送特殊参数 是否添加角标提醒
     */
    private boolean signRemind;

    /**
     * WEB推送特殊参数 推送类型
     */
    private WebPushTypeEnum webPushTypeEnum;

    public String getDeptId() {
        return deptId;
    }

    public void setDeptId(String deptId) {
        this.deptId = deptId;
    }

    public boolean isAddMsgList() {
        return addMsgList;
    }

    public void setAddMsgList(boolean addMsgList) {
        this.addMsgList = addMsgList;
    }

    public boolean isSignRemind() {
        return signRemind;
    }

    public void setSignRemind(boolean signRemind) {
        this.signRemind = signRemind;
    }

    public WebPushTypeEnum getWebPushTypeEnum() {
        return webPushTypeEnum;
    }

    public void setWebPushTypeEnum(WebPushTypeEnum webPushTypeEnum) {
        this.webPushTypeEnum = webPushTypeEnum;
    }

    public Message() {
    }

    public Message(String title, String content) {
        this.title = title;
        this.content = content;
    }

    public Message(String title, String content, Map<String, Object> bean) {
        this.title = title;
        this.content = content;
        this.bean = bean;
    }

    public Message(String userId,String deptId, String title, String content) {
        this.userId = userId;
        this.deptId = deptId;
        this.title = title;
        this.content = content;
    }

    public Message(String userId,String deptId, String title, String content, Map<String, Object> bean) {
        this.userId = userId;
        this.deptId = deptId;
        this.title = title;
        this.content = content;
        this.bean = bean;
    }

    public Message(String userId,String deptId, String title, String content, Map<String, Object> bean, JSONObject params) {
        this.userId = userId;
        this.deptId = deptId;
        this.title = title;
        this.content = content;
        this.bean = bean;
        this.params = params;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getButtonText() {
        return buttonText;
    }

    public void setButtonText(String buttonText) {
        this.buttonText = buttonText;
    }

    public String getButtonAction() {
        return buttonAction;
    }

    public void setButtonAction(String buttonAction) {
        this.buttonAction = buttonAction;
    }

    public Boolean getLogin() {
        return login;
    }

    public void setLogin(Boolean login) {
        this.login = login;
    }

    public Boolean getLoginHistory() {
        return loginHistory;
    }

    public void setLoginHistory(Boolean loginHistory) {
        this.loginHistory = loginHistory;
    }

    public Map<String, Object> getBean() {
        return bean;
    }

    public void setBean(Map<String, Object> bean) {
        this.bean = bean;
    }

    public void setBeanValue(String key, Object value) {
        this.bean.put(key, value);
    }

    public JSONObject getParams() {
        return params;
    }

    public void setParams(JSONObject params) {
        this.params = params;
    }

    public void setParamsValue(String key, Object value) {
        this.params.put(key, value);
    }

    public String getOnlySocket() {
        return onlySocket;
    }

    public void setOnlySocket(String onlySocket) {
        this.onlySocket = onlySocket;
    }

    public PayloadInfo getPayloadInfo() {
        return payloadInfo;
    }

    public void setPayloadInfo(PayloadInfo payloadInfo) {
        this.payloadInfo = payloadInfo;
    }

}