/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.message.service.email;

import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.je.common.base.util.StringUtil;
import com.je.message.vo.EmailMsg;
import javax.mail.internet.InternetAddress;

public interface EmailOperationService {
    /**
     * 发送Email
     *
     * @param msgVo       消息
     * @param
     * @return
     */
    void sendEmail(EmailMsg msgVo) throws Exception;

//    /**
//     * 构建Email配置
//     *
//     * @param
//     * @param msgVo       消息
//     * @return
//     */
//    Properties buildEmailConfig(EmailMsg msgVo);


    default InternetAddress[] buildAddress(String sjrStr) throws Exception {
        if (StringUtil.isNotEmpty(sjrStr)) {
            JSONArray arrays = JSONArray.parseArray(sjrStr);
            InternetAddress[] ias = new InternetAddress[arrays.size()];
            for (int i = 0; i < arrays.size(); i++) {
                JSONObject infos = arrays.getJSONObject(i);
                InternetAddress to = new InternetAddress(infos.getString("code"));
                to.setPersonal(infos.getString("text"));
                ias[i] = to;
            }
            return ias;
        } else {
            return null;
        }
    }

}
