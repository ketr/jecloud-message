/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.message.service.log.impl;

import com.google.common.base.Strings;
import com.je.common.base.DynaBean;
import com.je.common.base.service.rpc.BeanService;
import com.je.common.base.util.DateUtils;
import com.je.common.base.util.StringUtil;
import com.je.message.vo.LogMsg;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 *
 * @ClassName
 * @Author wangchao
 * @Date 2022/4/27 0027 9:55
 * @Version V1.0
 */
@Service("emailLogService")
public class EmailLogServiceImpl extends AbstractLogServiceImpl {

    @Override
    public void saveLog(LogMsg message) {
        DynaBean emailLog = new DynaBean("JE_SYS_EMAILLOG", false);
        emailLog.set(BeanService.KEY_PK_CODE, "JE_SYS_EMAILLOG_ID");
        emailLog.set("EMAILLOG_RECEIVEEMAIL", message.getReceiveEmail());
        emailLog.set("EMAILLOG_SUBJECT", message.getSubject());
        emailLog.set("EMAILLOG_CONTEXTTYPE", message.getContextType());
        emailLog.set("EMAILLOG_CONTEXT", message.getContext().length() > 500 ? message.getContext().substring(0, 500) : message.getContext());
        emailLog.set("EMAILLOG_FL", message.getFl());
        emailLog.set("EMAILLOG_SENDTIME", DateUtils.formatDateTime(new Date()));
        List<String> fileNames = message.getFileNames();
        List<String> fileAddress = message.getAddresses();
        if (fileNames.size() > 0 && fileNames.size() == fileAddress.size()) {
            String[] files = new String[fileNames.size()];
            for (Integer i = 0; i < fileNames.size(); i++) {
                files[i] = fileNames.get(i) + "*" + fileAddress.get(i);
            }
            emailLog.set("EMAILLOG_FILES", StringUtil.buildSplitString(files, ","));
        }

        if (Strings.isNullOrEmpty(message.getErrorMessage())) {
            emailLog.set("EMAILLOG_STATUS", "1");
        } else {
            emailLog.set("EMAILLOG_STATUS", "0");
            emailLog.set("EMAILLOG_FAILUREINFO", message.getErrorMessage());
        }

        commonService.buildModelCreateInfo(emailLog);
        metaService.insert(emailLog);
    }
}
