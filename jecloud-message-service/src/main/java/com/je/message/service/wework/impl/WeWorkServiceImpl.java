package com.je.message.service.wework.impl;

import com.alibaba.fastjson2.JSONObject;
import com.google.common.base.Strings;
import com.je.common.base.DynaBean;
import com.je.common.base.mapper.query.NativeQuery;
import com.je.common.base.service.MetaRbacService;
import com.je.message.service.wework.TokenInfo;
import com.je.message.service.wework.WeWorkService;
import com.je.message.service.wework.WxHttpUtil;
import com.je.message.vo.weWork.AbstractWeWorkNotice;
import com.je.message.vo.weWork.WeWorkActionCardNotice;
import com.je.message.vo.weWork.WeWorkTextNotice;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class WeWorkServiceImpl implements WeWorkService {

    private static final String ACCESS_TOKEN_URL = "%s/cgi-bin/gettoken?corpid=%s&corpsecret=%s";
    @Autowired
    private MetaRbacService metaRbacService;

    private static final Logger logger = LoggerFactory.getLogger(WeWorkServiceImpl.class);

    // 缓存已获取的 access token
    private static final Map<String, TokenInfo> tokenCache = new ConcurrentHashMap<>();

    @Override
    public void send(AbstractWeWorkNotice abstractWeWorkNotice) {
        List<DynaBean> dynaBeans = metaRbacService.selectByNativeQuery(
                NativeQuery.build().tableCode("JE_RBAC_WECHAT_CONFIG").eq("1", "1"));
        logger.info("----------------获取企业微信配置dynaBeans=" + dynaBeans.size());
        Map<String, DynaBean> weConfigs = new HashMap<>();
        DynaBean weConfig = null;
        for (DynaBean dynaBean : dynaBeans) {
            weConfig = dynaBean;
            weConfigs.put("CONFIG_AGENTID", dynaBean);
        }

        if (weConfig == null) {
            logger.info(String.format("根据agentid%s没有找到config信息！", abstractWeWorkNotice.getAgentid()));
            return;
        }

        if (Strings.isNullOrEmpty(abstractWeWorkNotice.getAgentid())) {
            abstractWeWorkNotice.setAgentid(weConfig.getStr("CONFIG_AGENTID"));
        }

        if (abstractWeWorkNotice instanceof WeWorkTextNotice) {
            WeWorkTextNotice weWorkTextNotice = (WeWorkTextNotice) abstractWeWorkNotice;
            sendTextMessage(weWorkTextNotice, weConfig);
        } else if (abstractWeWorkNotice instanceof WeWorkActionCardNotice) {
            WeWorkActionCardNotice weWorkActionCardNotice = (WeWorkActionCardNotice) abstractWeWorkNotice;
            buildUrl(weWorkActionCardNotice, weConfig);
            sendCarMessage(weWorkActionCardNotice, weConfig);
        }

    }


    private void buildUrl(WeWorkActionCardNotice weWorkActionCardNotice, DynaBean dynaBean) {
        String btnUrl = weWorkActionCardNotice.getUrl().replace("{@CONFIG_DOMAIN@}", dynaBean.getStr("CONFIG_DOMAIN"))
                .replace("{@CONFIG_APPID@}", dynaBean.getStr("CONFIG_AGENTID"));
        try {
            btnUrl = URLEncoder.encode(btnUrl, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String url = buildAuthorizationUrl(btnUrl, null, dynaBean.getStr("CONFIG_CORPID"), dynaBean.getStr("CONFIG_AGENTID"));
        weWorkActionCardNotice.setUrl(url);
    }

    public String buildAuthorizationUrl(String redirectUri, String state, String appid, String agentId) {
        String scope = "snsapi_privateinfo";
        StringBuilder url = new StringBuilder("https://open.weixin.qq.com/connect/oauth2/authorize");
        url.append("?appid=").append(appid);
        try {
            url.append("&redirect_uri=").append(URLEncoder.encode(redirectUri, "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        url.append("&response_type=code");
        url.append("&scope=").append(scope);
        if ("snsapi_privateinfo".equals(scope) || "snsapi_userinfo".equals(scope)) {
            url.append("&agentid=").append(agentId);
        }

        if (state != null) {
            url.append("&state=").append(state);
        }

        url.append("#wechat_redirect");
        return url.toString();
    }


    private void sendTextMessage(WeWorkTextNotice weWorkTextNotice, DynaBean weConfig) {
        JSONObject context = new JSONObject();
        //用户id 多个用|分割；
        context.put("touser", getWeWorkIds(weWorkTextNotice.getTouser()).get("userId"));
        //部门id  多个用|分割；
        context.put("toparty", weWorkTextNotice.getToparty());
        //标签id  多个用|分割；
        context.put("totag", weWorkTextNotice.getTotag());
        context.put("msgtype", "text");
        context.put("agentid", weWorkTextNotice.getAgentid());
        JSONObject textJson = new JSONObject();
        textJson.put("content", weWorkTextNotice.getContent());
        context.put("text", textJson);
        context.put("safe", "0");
        context.put("enable_duplicate_check", "1");
        send(context.toJSONString(), weConfig, weWorkTextNotice.getAgentid());
    }


    private void sendCarMessage(WeWorkActionCardNotice weWorkActionCardNotice, DynaBean weConfig) {
        JSONObject context = new JSONObject();
        //用户id 多个用|分割；
        context.put("touser", getWeWorkIds(weWorkActionCardNotice.getTouser()).get("userId"));
        //部门id  多个用|分割；
        context.put("toparty", weWorkActionCardNotice.getToparty());
        //标签id  多个用|分割；
        context.put("totag", weWorkActionCardNotice.getTotag());
        context.put("msgtype", "textcard");
        context.put("agentid", weWorkActionCardNotice.getAgentid());
        JSONObject textJson = new JSONObject();
        textJson.put("title", weWorkActionCardNotice.getTitle());
        textJson.put("description", weWorkActionCardNotice.getDescription());
        textJson.put("url", weWorkActionCardNotice.getUrl());
        textJson.put("btntxt", weWorkActionCardNotice.getBtntxt());
        context.put("textcard", textJson);
        context.put("enable_duplicate_check", "1");
        send(context.toJSONString(), weConfig, weWorkActionCardNotice.getAgentid());
    }

    private Map<String, String> getWeWorkIds(List<String> ids) {
        List<DynaBean> list = metaRbacService.selectByNativeQuery(NativeQuery.build().tableCode("JE_RBAC_ACCOUNT")
                .in("USER_ASSOCIATION_ID", ids));

        List<String> accountIds = new ArrayList<>();
        for (DynaBean dynaBean : list) {
            if (accountIds.contains(dynaBean.getStr("JE_RBAC_ACCOUNT_ID"))) {
                continue;
            }
            accountIds.add(dynaBean.getStr("JE_RBAC_ACCOUNT_ID"));
        }

        List<DynaBean> cpUsers = metaRbacService.selectByNativeQuery(NativeQuery.build().tableCode("JE_RBAC_CPUSER")
                .in("CPUSER_BDYH_ID", accountIds).eq("CPUSER_TYPE", "WECHAT"));

        List<String> cpUserIds = new ArrayList<>();
        List<String> cpUnIds = new ArrayList<>();
        for (DynaBean dynaBean : cpUsers) {
            logger.info("推送企业微信用户id=" + dynaBean.getStr("CPUSER_ZH"));
            if (cpUserIds.contains(dynaBean.getStr("CPUSER_ZH"))) {
                continue;
            }
            cpUserIds.add(dynaBean.getStr("CPUSER_ZH"));
            cpUnIds.add(dynaBean.getStr("CPUSER_UNIONID"));
        }
        Map<String, String> result = new HashMap<>();
        result.put("userId", String.join("|", cpUserIds));
        return result;
    }

    private void send(String body, DynaBean weConfig, String agentId) {
//        logger.info("发送企业微信消息Body=" + body);
        String appkey = weConfig.getStr("CONFIG_CORPID");

        List<DynaBean> list = metaRbacService.selectByTableCodeAndNativeQuery("JE_RBAC_WECHAT_APP",
                NativeQuery.build().eq("APP_ID", agentId));

        String appSecret = "";
        if (list.size() > 0) {
            appSecret = list.get(0).getStr("APP_SECRET");
        }

        if (Strings.isNullOrEmpty(appSecret)) {
            appSecret = weConfig.getStr("CONFIG_AGENTSECRET");
        }

        DynaBean dynaBean = metaRbacService.selectOneByNativeQuery("JE_RBAC_WECHAT_DOMAIN_NAME", null);
        String address = dynaBean.getStr("DEFAULT_CP_BASE_URL", "https://qyapi.weixin.qq.com");
        String proxyHost = dynaBean.getStr("NAME_DLDZ", "");
        String proxyPort = dynaBean.getStr("NAME_DLDK", "");

        String accessToken = getAccessToken(appkey, appSecret, address, proxyHost, proxyPort);

        String url = String.format("%s/cgi-bin/message/send?access_token=%s", address, accessToken);

        String result = WxHttpUtil.post(url, body, proxyHost, proxyPort);
        logger.info("--------------------------企业微信发送消息返回信息=" + result);
    }


    private static String getAccessToken(String appKey, String appSecret, String address, String proxyHost, String proxyPort) {
        // 检查缓存中是否存在对应的 appSecret
        if (tokenCache.containsKey(appSecret)) {
            TokenInfo tokenInfo = tokenCache.get(appSecret);
            // 如果 token 未过期，则直接返回缓存的 token
            if (tokenInfo != null && !isTokenExpired(tokenInfo)) {
                logger.info("从缓存中获取 access_token");
                return tokenInfo.getToken();
            }
        }

        // 缓存中不存在对应的 appSecret，或者 token 已过期，调用接口获取 token
        String result = WxHttpUtil.get(String.format(ACCESS_TOKEN_URL, address, appKey, appSecret), proxyHost, proxyPort);
        if (Strings.isNullOrEmpty(result)) {
            logger.info(String.format("企业微信获取access_token异常,appKey = %s  , appSecret = %s", appKey, appSecret));
            throw new RuntimeException("企业微信获取access_token异常");
        }
        JSONObject resutJson = JSONObject.parseObject(result);
        String errcode = resutJson.getString("errcode");
        if (!errcode.equals("0")) {
            logger.info(String.format("企业微信获取access_token异常,appKey = %s  , appSecret = %s", appKey, appSecret));
            throw new RuntimeException(String.format("企业微信获取access_token异常,异常码=%s,异常信息=%s", errcode
                    , resutJson.getString("errmsg")));
        }

        String token = resutJson.getString("access_token");
        int expiresIn = resutJson.getIntValue("expires_in");//7200秒

        // 缓存获取到的 token
        TokenInfo tokenInfo = new TokenInfo(token, expiresIn);
        tokenCache.put(appSecret, tokenInfo);

        return token;
    }

    // 检查 token 是否过期
    private static boolean isTokenExpired(TokenInfo tokenInfo) {
        long currentTime = System.currentTimeMillis() / 1000; // 当前时间戳（单位：秒）
        return (currentTime - tokenInfo.getCreateTime()) >= (tokenInfo.getExpiresIn() - 2000);
    }

}
