/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.message.service.email;

import com.je.message.vo.EmailMsg;

import java.util.*;

/**
 * Email服务定义
 */
public interface EmailService {

    /**
     * 发送消息
     *
     * @param msgVo 消息VO实例
     */
    void send(EmailMsg msgVo);

    /**
     * 发送邮件消息
     *
     * @param receiveEmail 接受邮箱
     * @param subject      主题
     * @param contextType  发送内容类型   文本：SendContextType.TEXT HTML:SendContextType.HTML
     * @param context      发送内容
     */
    void send(String receiveEmail, String subject, String contextType, String context);

    /**
     * 按照模版发送邮件
     *
     * @param receiveEmail 接受的邮箱
     * @param code         编码
     * @param params       传入信息
     */
    void send(String receiveEmail, String code, Map<String, Object> params);

    /**
     * 发送带有附件的邮件
     *
     * @param receiveEmail 接受邮箱
     * @param subject      主题
     * @param contextType  内容类型   文本：SendContextType.TEXT HTML:SendContextType.HTML
     * @param context      内容
     * @param fileNames    文件名
     * @param addresses    文件地址
     */
    void send(String receiveEmail, String subject, String contextType, String context, List<String> fileNames, List<String> addresses);


}
