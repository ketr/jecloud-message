package com.je.message.service.dingtalk;


import com.alibaba.fastjson2.JSONObject;
import com.aliyun.dingtalktodo_1_0.models.CreateTodoTaskRequest;

import java.util.List;

public interface DingTalkTaskService {
    /**
     * 创建待办任务
     *
     * @throws Exception
     */
    void createTodo(String accessToken, String pkValue, String unionId, String toUserId,
                    List<CreateTodoTaskRequest.CreateTodoTaskRequestContentFieldList> contentFieldList,
                    List<CreateTodoTaskRequest.CreateTodoTaskRequestActionList> actionList,
                    CreateTodoTaskRequest.CreateTodoTaskRequestDetailUrl detailUrl, String jeCloudId, String subject) throws Exception;

    /**
     * 根据待办任务，获取钉钉待办任务详情
     *
     * @param taskId
     * @param accessToken
     * @param userId
     * @return
     * @throws Exception
     */
    JSONObject getTodoInfo(String taskId, String accessToken, String userId, String jeCloudId) throws Exception;

    /**
     * 根据服务端API-查询企业下用户待办列表接口，获取企业用户待办列表。
     */
    void getOrgTodoList(String accessToken) throws Exception;

    /**
     * 根据待办任务id，调用服务端API-更新钉钉待办任务接口，更新待办任务信息及状态。
     */

    void updateTodo(String accessToken, String toDoPersonnel, String personnelAlreadyHandled, String pkValue, String unionId, String jeCloudId) throws Exception;

    void completeTodo(String accessToken, String pkValue, String unionId, String jeCloudId) throws Exception;

    /**
     * 根据待办任务id，调用服务端API-删除钉钉待办任务接口，删除待办任务信息。
     */

    void deleteTodo(String accessToken, String userId, String pkValue, String jeCloudId) throws Exception;


}