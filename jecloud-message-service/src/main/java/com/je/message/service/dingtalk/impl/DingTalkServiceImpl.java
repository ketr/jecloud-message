/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.message.service.dingtalk.impl;

import com.alibaba.fastjson2.JSONObject;
import com.aliyun.dingtalktodo_1_0.models.CreateTodoTaskRequest;
import com.dingtalk.api.DefaultDingTalkClient;
import com.dingtalk.api.DingTalkClient;
import com.dingtalk.api.request.OapiGettokenRequest;
import com.dingtalk.api.request.OapiMessageCorpconversationAsyncsendV2Request;
import com.dingtalk.api.response.OapiGettokenResponse;
import com.dingtalk.api.response.OapiMessageCorpconversationAsyncsendV2Response;
import com.google.common.base.Strings;
import com.je.common.base.DynaBean;
import com.je.common.base.mapper.query.NativeQuery;
import com.je.common.base.service.MetaRbacService;
import com.je.message.service.dingtalk.DingTalkService;
import com.je.message.service.dingtalk.DingTalkTaskService;
import com.je.message.vo.dingTalk.*;
import com.taobao.api.ApiException;
import com.taobao.api.TaobaoObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.*;

@Service
public class DingTalkServiceImpl implements DingTalkService {

    @Autowired
    private MetaRbacService metaRbacService;
    @Autowired
    private DingTalkTaskService dingTalkTaskService;

    private static final Logger logger = LoggerFactory.getLogger(DingTalkServiceImpl.class);

    private static final String MESSAGE_SERVER_URL = "https://oapi.dingtalk.com/topapi/message/corpconversation/asyncsend_v2";

    private static final String ACCESS_TOKEN_URL = "https://oapi.dingtalk.com/gettoken";

    @Override
    public void send(DingTalkMsg msgVo, AbstractDingTalkWorkNotice abstractDingTalkWorkNotice) {
        String id = msgVo.getJeCloudDingTalkId();
        List<DynaBean> dynaBeans = metaRbacService.selectByNativeQuery(
                NativeQuery.build().tableCode("JE_RBAC_DINGTALK_CONFIG").in("JE_RBAC_DINGTALK_CONFIG_ID", id.split(",")));
        for (DynaBean dynaBean : dynaBeans) {
            TaobaoObject obj = null;
            if (abstractDingTalkWorkNotice instanceof DingTalkTextWorkNotice) {
                OapiMessageCorpconversationAsyncsendV2Request.Text text = new OapiMessageCorpconversationAsyncsendV2Request.Text();
                DingTalkTextWorkNotice dingTalkTextWorkNotice = (DingTalkTextWorkNotice) abstractDingTalkWorkNotice;
                text.setContent(dingTalkTextWorkNotice.getContent());
                obj = text;
            } else if (abstractDingTalkWorkNotice instanceof DingTalkActionCardWorkNotice) {
                OapiMessageCorpconversationAsyncsendV2Request.ActionCard actionCard = new OapiMessageCorpconversationAsyncsendV2Request.ActionCard();
                DingTalkActionCardWorkNotice dingTalkActionCardWorkNotice = (DingTalkActionCardWorkNotice) abstractDingTalkWorkNotice;
                actionCard.setTitle(dingTalkActionCardWorkNotice.getTitle());
                actionCard.setMarkdown(dingTalkActionCardWorkNotice.getContent());
                if (dingTalkActionCardWorkNotice.getBtnInfoList().size() > 1) {
                    List<OapiMessageCorpconversationAsyncsendV2Request.BtnJsonList> btnJsonList = new ArrayList<>();
                    for (DingTalkActionCardWorkNotice.BtnInfo btnInfo : dingTalkActionCardWorkNotice.getBtnInfoList()) {
                        OapiMessageCorpconversationAsyncsendV2Request.BtnJsonList btnJsonList1 = new OapiMessageCorpconversationAsyncsendV2Request.BtnJsonList();
                        btnJsonList1.setTitle(btnInfo.getTitle());
                        btnJsonList1.setActionUrl(btnInfo.getUrl()
                                .replace("{@CONFIG_DOMAIN@}", dynaBean.getStr("CONFIG_DOMAIN"))
                                .replace("{@CONFIG_APPID@}", dynaBean.getStr("CONFIG_APPID"))
                        );
                        btnJsonList.add(btnJsonList1);
                    }
                    actionCard.setBtnJsonList(btnJsonList);
                } else {
                    DingTalkActionCardWorkNotice.BtnInfo btnInfo = dingTalkActionCardWorkNotice.getBtnInfoList().get(0);
                    actionCard.setSingleTitle(btnInfo.getTitle());

                    String CONFIG_XXDKLX = Strings.isNullOrEmpty(dynaBean.getStr("CONFIG_XXDKLX")) ? "2" : dynaBean.getStr("CONFIG_XXDKLX");
                    String btnUrl = btnInfo.getUrl().replace("{@CONFIG_DOMAIN@}", dynaBean.getStr("CONFIG_DOMAIN"))
                            .replace("{@CONFIG_APPID@}", dynaBean.getStr("CONFIG_APPID"));
                    btnUrl += "&configXxdklx=" + CONFIG_XXDKLX;

                    if (CONFIG_XXDKLX.equals("3") || CONFIG_XXDKLX.equals("1")) {
                        try {
                            btnUrl = URLEncoder.encode(btnUrl, "UTF-8");
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }

                    String url = "";
                    if (CONFIG_XXDKLX.equals("3")) {//工作台打开
                        url = String.format("dingtalk://dingtalkclient/action/openapp?corpid=%s&container_type" +
                                        "=work_platform&app_id=0_%s&redirect_type=jump&redirect_url=%s"
                                , dynaBean.getStr("CONFIG_CORPID"), dynaBean.getStr("CONFIG_APPID"), btnUrl);
                    } else if (CONFIG_XXDKLX.equals("1")) {//浏览器打开
                        url = String.format("dingtalk://dingtalkclient/page/link?url=%s&pc_slide=true", btnUrl);
                    } else {
                        url = btnUrl;
                    }

                    logger.info("钉钉发送工作同时，朓转地址-----------------" + url);
                    actionCard.setSingleUrl(url);
                }

                obj = actionCard;
            } else if (abstractDingTalkWorkNotice instanceof DingTalkMarkDownWorkNotice) {
                OapiMessageCorpconversationAsyncsendV2Request.Markdown markdown = new OapiMessageCorpconversationAsyncsendV2Request.Markdown();
                DingTalkMarkDownWorkNotice dingTalkMarkDownWorkNotice = (DingTalkMarkDownWorkNotice) abstractDingTalkWorkNotice;
                markdown.setTitle(dingTalkMarkDownWorkNotice.getTitle());
                markdown.setText("【" + dingTalkMarkDownWorkNotice.getTitle() + "】<br/>" + dingTalkMarkDownWorkNotice.getContent());
                obj = markdown;
            }
            try {
                String appKey = dynaBean.getStr("CONFIG_APPKEY");
                String appSecret = dynaBean.getStr("CONFIG_APPSECRET");
                String agentId = dynaBean.getStr("CONFIG_APPID");
                String jeCloudDingId = dynaBean.getStr("JE_RBAC_DINGTALK_CONFIG_ID");
                sendCardMsg(obj, getDingTalkIds(msgVo.getToUserIds(), jeCloudDingId), appKey,
                        appSecret, agentId);
            } catch (ApiException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void createTodo(String jeCloudDingId, String pkValue, String userIds, String jeCloudUrl, List<DingTalkTaskField> fields, String startUserId, String subject) {
        List<DynaBean> dynaBeans = metaRbacService.selectByNativeQuery(
                NativeQuery.build().tableCode("JE_RBAC_DINGTALK_CONFIG").in("JE_RBAC_DINGTALK_CONFIG_ID", jeCloudDingId.split(",")));
        Map<String, String> tokens = new HashMap<>();
        for (DynaBean dynaBean : dynaBeans) {
            String CONFIG_XXDKLX = Strings.isNullOrEmpty(dynaBean.getStr("CONFIG_XXDKLX")) ? "2" : dynaBean.getStr("CONFIG_XXDKLX");
            String url = "";

            jeCloudUrl = jeCloudUrl.replace("{@CONFIG_DOMAIN@}", dynaBean.getStr("CONFIG_DOMAIN"))
                    .replace("{@CONFIG_APPID@}", dynaBean.getStr("CONFIG_APPID"));
            jeCloudUrl += "&configXxdklx=" + CONFIG_XXDKLX;
            if (CONFIG_XXDKLX.equals("3") || CONFIG_XXDKLX.equals("1")) {
                try {
                    jeCloudUrl = URLEncoder.encode(jeCloudUrl, "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }

            if (CONFIG_XXDKLX.equals("3")) {//工作台打开
                url = String.format("dingtalk://dingtalkclient/action/openapp?corpid=%s&container_type" +
                                "=work_platform&app_id=0_%s&redirect_type=jump&redirect_url=%s"
                        , dynaBean.getStr("CONFIG_CORPID"), dynaBean.getStr("CONFIG_APPID"), jeCloudUrl);
            } else if (CONFIG_XXDKLX.equals("1")) {//浏览器打开
                url = String.format("dingtalk://dingtalkclient/page/link?url=%s&pc_slide=true", jeCloudUrl);
            } else {
                url = jeCloudUrl;
            }

            logger.info("创建待办-----------------" + url);
            String appKey = dynaBean.getStr("CONFIG_APPKEY");
            String appSecret = dynaBean.getStr("CONFIG_APPSECRET");
            String agentId = dynaBean.getStr("CONFIG_APPID");
            String id = dynaBean.getStr("JE_RBAC_DINGTALK_CONFIG_ID");

            //表单字段展示
            List<CreateTodoTaskRequest.CreateTodoTaskRequestContentFieldList> contentFieldList = new ArrayList<>();
            for (DingTalkTaskField field : fields) {
                CreateTodoTaskRequest.CreateTodoTaskRequestContentFieldList fieldList = new CreateTodoTaskRequest.CreateTodoTaskRequestContentFieldList();
                fieldList.setFieldKey(field.getKey());
                fieldList.setFieldValue(field.getValue());
                contentFieldList.add(fieldList);
            }

            List<CreateTodoTaskRequest.CreateTodoTaskRequestActionList> actionList = new ArrayList<>();
            CreateTodoTaskRequest.CreateTodoTaskRequestActionList actionList1 = new CreateTodoTaskRequest.CreateTodoTaskRequestActionList();
            actionList1.setPcUrl(url);
            actionList1.setUrl(url);
            actionList1.setTitle("点击查看");
            actionList.add(actionList1);

            CreateTodoTaskRequest.CreateTodoTaskRequestDetailUrl detailUrl = new CreateTodoTaskRequest.CreateTodoTaskRequestDetailUrl();
            detailUrl.setAppUrl(url);
            detailUrl.setPcUrl(url);

            try {
                if (!tokens.containsKey(appKey)) {
                    tokens.put(appKey, buildAccessToken(appKey, appSecret));
                }
                String token = tokens.get(appKey);

                String unids = getDingTalkIds(Arrays.asList(userIds.split(",")), id).get("unId");

                List<String> startUserIdArray = new ArrayList<>();
                startUserIdArray.add(startUserId);
                String startUserUnid = getDingTalkIds(startUserIdArray, id).get("unId");
                if (startUserUnid.indexOf(",") > 0) {
                    startUserUnid = startUserUnid.split(",")[0];
                }
                dingTalkTaskService.createTodo(token, pkValue, startUserUnid, unids, contentFieldList, actionList, detailUrl, id, subject);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    }

    @Override
    public void updateTodo(String jeCloudDingId, String toDoPersonnel, String personnelAlreadyHandled, String pkValue, String startUserId) {
        List<DynaBean> dynaBeans = metaRbacService.selectByNativeQuery(
                NativeQuery.build().tableCode("JE_RBAC_DINGTALK_CONFIG").in("JE_RBAC_DINGTALK_CONFIG_ID", jeCloudDingId.split(",")));
        Map<String, String> tokens = new HashMap<>();
        for (DynaBean dynaBean : dynaBeans) {
            try {
                String appKey = dynaBean.getStr("CONFIG_APPKEY");
                String appSecret = dynaBean.getStr("CONFIG_APPSECRET");
                String agentId = dynaBean.getStr("CONFIG_APPID");
                String id = dynaBean.getStr("JE_RBAC_DINGTALK_CONFIG_ID");
                if (!tokens.containsKey(appKey)) {
                    tokens.put(appKey, buildAccessToken(appKey, appSecret));
                }
                String token = tokens.get(appKey);
                String toDoPersonnelUnids = getDingTalkIds(Arrays.asList(toDoPersonnel.split(",")), id).get("unId");
                String personnelAlreadyHandledUnids = getDingTalkIds(Arrays.asList(personnelAlreadyHandled.split(",")), id).get("unId");
                List<String> startUserIdArray = new ArrayList<>();
                startUserIdArray.add(startUserId);
                String startUserUnid = getDingTalkIds(startUserIdArray, id).get("unId");
                if (startUserUnid.indexOf(",") > 0) {
                    startUserUnid = startUserUnid.split(",")[0];
                }
                dingTalkTaskService.updateTodo(token, toDoPersonnelUnids, personnelAlreadyHandledUnids, pkValue, startUserUnid, id);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


    }

    @Override
    public List<JSONObject> getTodoInfo(String taskId, String jeCloudDingId, String userId) {
        List<JSONObject> result = new ArrayList<>();
        List<DynaBean> dynaBeans = metaRbacService.selectByNativeQuery(
                NativeQuery.build().tableCode("JE_RBAC_DINGTALK_CONFIG").in("JE_RBAC_DINGTALK_CONFIG_ID", jeCloudDingId.split(",")));
        Map<String, String> tokens = new HashMap<>();
        for (DynaBean dynaBean : dynaBeans) {
            try {
                String appKey = dynaBean.getStr("CONFIG_APPKEY");
                String appSecret = dynaBean.getStr("CONFIG_APPSECRET");
                String agentId = dynaBean.getStr("CONFIG_APPID");
                String id = dynaBean.getStr("JE_RBAC_DINGTALK_CONFIG_ID");
                if (!tokens.containsKey(appKey)) {
                    tokens.put(appKey, buildAccessToken(appKey, appSecret));
                }
                String token = tokens.get(appKey);
                List<String> startUserIdArray = new ArrayList<>();
                startUserIdArray.add(userId);
                String startUserUnid = getDingTalkIds(startUserIdArray, id).get("unId");
                if (startUserUnid.indexOf(",") > 0) {
                    startUserUnid = startUserUnid.split(",")[0];
                }
                result.add(dingTalkTaskService.getTodoInfo(taskId, token, startUserUnid, id));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    @Override
    public void deleteTodo(String jeCloudDingId, String userId, String taskId) {
        List<JSONObject> result = new ArrayList<>();
        List<DynaBean> dynaBeans = metaRbacService.selectByNativeQuery(
                NativeQuery.build().tableCode("JE_RBAC_DINGTALK_CONFIG").in("JE_RBAC_DINGTALK_CONFIG_ID", jeCloudDingId.split(",")));
        Map<String, String> tokens = new HashMap<>();
        for (DynaBean dynaBean : dynaBeans) {
            try {
                String appKey = dynaBean.getStr("CONFIG_APPKEY");
                String appSecret = dynaBean.getStr("CONFIG_APPSECRET");
                String agentId = dynaBean.getStr("CONFIG_APPID");
                String id = dynaBean.getStr("JE_RBAC_DINGTALK_CONFIG_ID");
                if (!tokens.containsKey(appKey)) {
                    tokens.put(appKey, buildAccessToken(appKey, appSecret));
                }
                String token = tokens.get(appKey);
                List<String> startUserIdArray = new ArrayList<>();
                startUserIdArray.add(userId);
                String startUserUnid = getDingTalkIds(startUserIdArray, id).get("unId");
                if (startUserUnid.indexOf(",") > 0) {
                    startUserUnid = startUserUnid.split(",")[0];
                }
                dingTalkTaskService.deleteTodo(token, startUserUnid, taskId, id);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void completeTodo(String jeCloudDingId, String pkValue, String userId) {
        List<DynaBean> dynaBeans = metaRbacService.selectByNativeQuery(
                NativeQuery.build().tableCode("JE_RBAC_DINGTALK_CONFIG").in("JE_RBAC_DINGTALK_CONFIG_ID", jeCloudDingId.split(",")));
        Map<String, String> tokens = new HashMap<>();
        for (DynaBean dynaBean : dynaBeans) {
            try {
                String appKey = dynaBean.getStr("CONFIG_APPKEY");
                String appSecret = dynaBean.getStr("CONFIG_APPSECRET");
                String agentId = dynaBean.getStr("CONFIG_APPID");
                String id = dynaBean.getStr("JE_RBAC_DINGTALK_CONFIG_ID");
                if (!tokens.containsKey(appKey)) {
                    tokens.put(appKey, buildAccessToken(appKey, appSecret));
                }
                String token = tokens.get(appKey);
                List<String> startUserIdArray = new ArrayList<>();
                startUserIdArray.add(userId);
                String startUserUnid = getDingTalkIds(startUserIdArray, id).get("unId");
                if (startUserUnid.indexOf(",") > 0) {
                    startUserUnid = startUserUnid.split(",")[0];
                }
                dingTalkTaskService.completeTodo(token, pkValue, startUserUnid, id);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


    }


    public OapiMessageCorpconversationAsyncsendV2Response sendCardMsg(TaobaoObject message, Map<String, String> user,
                                                                      String appKey, String appSecret, String agentId) throws ApiException {
        DingTalkClient client = new DefaultDingTalkClient(MESSAGE_SERVER_URL);
        String token = buildAccessToken(appKey, appSecret);
        OapiMessageCorpconversationAsyncsendV2Request req = new OapiMessageCorpconversationAsyncsendV2Request();
        req.setAgentId(Long.parseLong(agentId));
        req.setUseridList(user.get("userId"));
        //构建消息对象
        OapiMessageCorpconversationAsyncsendV2Request.Msg msg = new OapiMessageCorpconversationAsyncsendV2Request.Msg();
        if (message instanceof OapiMessageCorpconversationAsyncsendV2Request.ActionCard) {
            msg.setActionCard((OapiMessageCorpconversationAsyncsendV2Request.ActionCard) message);
            msg.setMsgtype("action_card");
            req.setMsg(msg);
        } else if (message instanceof OapiMessageCorpconversationAsyncsendV2Request.Markdown) {
            msg.setMarkdown((OapiMessageCorpconversationAsyncsendV2Request.Markdown) message);
            msg.setMsgtype("markdown");
            req.setMsg(msg);
        } else if (message instanceof OapiMessageCorpconversationAsyncsendV2Request.Text) {
            msg.setText((OapiMessageCorpconversationAsyncsendV2Request.Text) message);
            msg.setMsgtype("text");
            req.setMsg(msg);
        }
        OapiMessageCorpconversationAsyncsendV2Response rsp = client.execute(req, token);
        return rsp;
    }


    private Map<String, String> getDingTalkIds(List<String> jeCloudUserIds, String foreignKey) {
        List<DynaBean> list = metaRbacService.selectByNativeQuery(NativeQuery.build().tableCode("JE_RBAC_ACCOUNT")
                .in("USER_ASSOCIATION_ID", jeCloudUserIds));

        List<String> accountIds = new ArrayList<>();
        for (DynaBean dynaBean : list) {
            logger.info("JECLOUD用户id=" + dynaBean.getStr("JE_RBAC_ACCOUNT_ID"));
            if (accountIds.contains(dynaBean.getStr("JE_RBAC_ACCOUNT_ID"))) {
                continue;
            }
            logger.info("JECLOUD用户2id=" + dynaBean.getStr("JE_RBAC_ACCOUNT_ID"));
            accountIds.add(dynaBean.getStr("JE_RBAC_ACCOUNT_ID"));
        }

        List<DynaBean> cpUsers = metaRbacService.selectByNativeQuery(NativeQuery.build().tableCode("JE_RBAC_CPUSER")
                .in("CPUSER_BDYH_ID", accountIds).eq("CPUSER_TYPE", "DINGTALK")
                .eq("CPUSER_FOREIGNKEY", foreignKey));
        logger.info("foreignKey=" + foreignKey);
        List<String> cpUserIds = new ArrayList<>();
        List<String> cpUnIds = new ArrayList<>();
        for (DynaBean dynaBean : cpUsers) {
            logger.info("推送钉钉用户id=" + dynaBean.getStr("CPUSER_ZH"));
            if (cpUserIds.contains(dynaBean.getStr("CPUSER_ZH"))) {
                continue;
            }
            cpUserIds.add(dynaBean.getStr("CPUSER_ZH"));
            cpUnIds.add(dynaBean.getStr("CPUSER_UNIONID"));
        }
        Map<String, String> result = new HashMap<>();
        result.put("userId", String.join(",", cpUserIds));
        result.put("unId", String.join(",", cpUnIds));
        return result;
    }

    private String buildAccessToken(String appKey, String appSecret) throws ApiException {
        //获取token
        DefaultDingTalkClient client = new DefaultDingTalkClient(ACCESS_TOKEN_URL);
        OapiGettokenRequest request = new OapiGettokenRequest();
        request.setAppkey(appKey);
        request.setAppsecret(appSecret);
        request.setHttpMethod("GET");
        OapiGettokenResponse response = client.execute(request);
        return response.getAccessToken();
    }

}
