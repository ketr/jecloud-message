/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.message.service.note;

import com.je.common.base.service.rpc.SystemSettingRpcService;
import com.je.common.base.spring.SpringContextHolder;
import com.je.common.base.util.MessageUtils;
import com.je.message.exception.MessageSendException;
import com.je.message.rpc.NoteRpcService;

/**
 * 获取短信服务接口
 */
public class NoteServiceFactory {

    public static final String SYSTEM_VAR_NOTETYPE = "JE_SYS_NOTEDIY";

    public static final String SYSTEM_VAR_DIY_NOTE_SERVICE = "JE_SYS_NOTEDIY_SERVICE";


    /**
     * 获取短信服务接口
     *
     * @return
     */
    public static NoteRpcService getNoteService() {
        SystemSettingRpcService metaSystemSettingRpcService = SpringContextHolder.getBean(SystemSettingRpcService.class);
        String noteType = metaSystemSettingRpcService.findSettingValue(SYSTEM_VAR_NOTETYPE);
        if (NoteTypeEnum.NOTEDIY.getName().equals(noteType)) {
            String serviceName = metaSystemSettingRpcService.findSettingValue(SYSTEM_VAR_DIY_NOTE_SERVICE);
            return SpringContextHolder.getBean(serviceName);
        }
        if (NoteTypeEnum.NOTE253.getName().equals(noteType)) {
            return SpringContextHolder.getBean("note253Service");
        } else if (NoteTypeEnum.NOTESYS.getName().equals(noteType)) {
            return SpringContextHolder.getBean("noteWebChineseService");
        } else if (NoteTypeEnum.NOTEALIYUN.getName().equals(noteType)) {
            return SpringContextHolder.getBean("noteAliyunService");
        }
        return null;
    }

    /**
     * 获取自定义短信服务接口
     *
     * @param diyService
     * @return
     */
    public static NoteRpcService getDiyNoteService(String diyService) {
        SystemSettingRpcService metaSystemSettingRpcService = SpringContextHolder.getBean(SystemSettingRpcService.class);
        String noteType = metaSystemSettingRpcService.findSettingValue(SYSTEM_VAR_NOTETYPE);
        if (!NoteTypeEnum.NOTEDIY.getName().equals(noteType)) {
            throw new MessageSendException(MessageUtils.getMessage("message.interface.type.mismatch"));
        }
        return SpringContextHolder.getBean(diyService);
    }
}
