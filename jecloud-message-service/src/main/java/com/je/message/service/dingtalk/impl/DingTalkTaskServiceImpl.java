package com.je.message.service.dingtalk.impl;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import com.aliyun.dingtalktodo_1_0.Client;
import com.aliyun.dingtalktodo_1_0.models.*;
import com.aliyun.tea.TeaException;
import com.aliyun.teautil.models.RuntimeOptions;
import com.google.common.base.Strings;
import com.je.common.base.DynaBean;
import com.je.common.base.service.MetaService;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.message.service.dingtalk.DingTalkTaskService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 钉钉相关业务实现
 *
 * @author wangmm@ketr.com.cn
 * @date 2020/4/14
 */
@Service
public class DingTalkTaskServiceImpl implements DingTalkTaskService {

    @Autowired
    private MetaService metaService;

    private static final Logger logger = LoggerFactory.getLogger(DingTalkTaskServiceImpl.class);

    @Override
    public void createTodo(String accessToken, String pkValue, String unionId, String toUserId,
                           List<CreateTodoTaskRequest.CreateTodoTaskRequestContentFieldList> contentFieldList,
                           List<CreateTodoTaskRequest.CreateTodoTaskRequestActionList> actionList,
                           CreateTodoTaskRequest.CreateTodoTaskRequestDetailUrl detailUrl, String jeCloudId,
                           String subject) throws Exception {
        deleteTodo(accessToken, unionId, pkValue, jeCloudId);
        Client client = DingTalkTaskServiceImpl.createClient();
        CreateTodoTaskHeaders createTodoTaskHeaders = new CreateTodoTaskHeaders();
        createTodoTaskHeaders.xAcsDingtalkAccessToken = accessToken;
        CreateTodoTaskRequest createTodoTaskRequest = new CreateTodoTaskRequest()
                .setSourceId(pkValue)
                .setSubject(subject)
//                .setActionList(actionList)
                .setExecutorIds(Arrays.asList(toUserId.split(",")))
                .setContentFieldList(contentFieldList)
                .setDetailUrl(detailUrl)
                .setIsOnlyShowExecutor(false);
        try {
            CreateTodoTaskResponse createTodo = client.createTodoTaskWithOptions(unionId,
                    createTodoTaskRequest, createTodoTaskHeaders, new RuntimeOptions());

            String id = createTodo.getBody().getId();
            DynaBean dynaBean = new DynaBean("JE_MESSAGE_DINGTALK_TODO", true);
            dynaBean.setStr("TODO_DING_TASK_ID", id);
            dynaBean.setStr("BUSINESS_KEY", pkValue);
            dynaBean.setStr("TODO_JECLOUDDINGID", jeCloudId);
            metaService.insert(dynaBean);
            logger.info("发送成功返回信息====================" + JSON.toJSONString(createTodo.getBody()));
        } catch (TeaException err) {
            if (!com.aliyun.teautil.Common.empty(err.code) && !com.aliyun.teautil.Common.empty(err.message)) {
                // err 中含有 code 和 message 属性，可帮助开发定位问题
                logger.info(err.code);
                logger.info(err.message);
            }
        } catch (Exception _err) {
            TeaException err = new TeaException(_err.getMessage(), _err);
            if (!com.aliyun.teautil.Common.empty(err.code) && !com.aliyun.teautil.Common.empty(err.message)) {
                // err 中含有 code 和 message 属性，可帮助开发定位问题
                logger.info(err.code);
                logger.info(err.message);
            }
        }
    }


    public static com.aliyun.dingtalktodo_1_0.Client createClient() throws Exception {
        com.aliyun.teaopenapi.models.Config config = new com.aliyun.teaopenapi.models.Config();
        config.protocol = "https";
        config.regionId = "central";
        return new com.aliyun.dingtalktodo_1_0.Client(config);
    }

//    @Override
//    public void updateTodo(String accessToken, String toDoPersonnel, String personnelAlreadyHandled, String pkValue, String unionId) throws Exception {
//        com.aliyun.dingtalktodo_1_0.Client client = DingTalkTaskServiceImpl.createClient();
//        UpdateTodoTaskExecutorStatusHeaders updateTodoTaskExecutorStatusHeaders = new UpdateTodoTaskExecutorStatusHeaders();
//        updateTodoTaskExecutorStatusHeaders.xAcsDingtalkAccessToken = accessToken;
//
//        List<UpdateTodoTaskExecutorStatusRequest.
//                UpdateTodoTaskExecutorStatusRequestExecutorStatusList> executorStatusList = new ArrayList<>();
//        if (!Strings.isNullOrEmpty(toDoPersonnel)) {
//            for (String user : toDoPersonnel.split(",")) {
//                UpdateTodoTaskExecutorStatusRequest.UpdateTodoTaskExecutorStatusRequestExecutorStatusList
//                        executorStatusList0 = new UpdateTodoTaskExecutorStatusRequest.
//                        UpdateTodoTaskExecutorStatusRequestExecutorStatusList()
//                        .setIsDone(false)
//                        .setId(user);
//                executorStatusList.add(executorStatusList0);
//            }
//        }
//
//        if (!Strings.isNullOrEmpty(personnelAlreadyHandled)) {
//            for (String user : personnelAlreadyHandled.split(",")) {
//                UpdateTodoTaskExecutorStatusRequest.UpdateTodoTaskExecutorStatusRequestExecutorStatusList
//                        executorStatusList0 = new UpdateTodoTaskExecutorStatusRequest.
//                        UpdateTodoTaskExecutorStatusRequestExecutorStatusList()
//                        .setIsDone(true)
//                        .setId(user);
//                executorStatusList.add(executorStatusList0);
//            }
//        }
//
//        UpdateTodoTaskExecutorStatusRequest updateTodoTaskExecutorStatusRequest = new UpdateTodoTaskExecutorStatusRequest()
//                .setExecutorStatusList(executorStatusList);
//        try {
//            DynaBean dynaBean = metaService.selectOne("JE_MESSAGE_DINGTALK_TODO", ConditionsWrapper.builder().eq("BUSINESS_KEY", pkValue));
//            UpdateTodoTaskExecutorStatusResponse c = client.updateTodoTaskExecutorStatusWithOptions(unionId, dynaBean.getStr("TODO_DING_TASK_ID"), updateTodoTaskExecutorStatusRequest,
//                    updateTodoTaskExecutorStatusHeaders, new com.aliyun.teautil.models.RuntimeOptions());
//            logger.info("发送成功返回信息====================" + JSON.toJSONString(c.getBody()));
//        } catch (TeaException err) {
//            if (!com.aliyun.teautil.Common.empty(err.code) && !com.aliyun.teautil.Common.empty(err.message)) {
//                // err 中含有 code 和 message 属性，可帮助开发定位问题
//            }
//
//        } catch (Exception _err) {
//            TeaException err = new TeaException(_err.getMessage(), _err);
//            if (!com.aliyun.teautil.Common.empty(err.code) && !com.aliyun.teautil.Common.empty(err.message)) {
//                // err 中含有 code 和 message 属性，可帮助开发定位问题
//            }
//
//        }
//    }


    @Override
    public void updateTodo(String accessToken, String toDoPersonnel, String personnelAlreadyHandled, String pkValue, String unionId, String jeCloudId) throws Exception {
        Client client = DingTalkTaskServiceImpl.createClient();
        com.aliyun.dingtalktodo_1_0.models.UpdateTodoTaskHeaders updateTodoTaskHeaders = new com.aliyun.dingtalktodo_1_0.models.UpdateTodoTaskHeaders();
        updateTodoTaskHeaders.xAcsDingtalkAccessToken = accessToken;

        List<String> toDoPersonnelList = new ArrayList<>();
        if (!Strings.isNullOrEmpty(toDoPersonnel)) {
            toDoPersonnelList = Arrays.asList(toDoPersonnel.split(","));
        }
        List<String> personnelAlreadyHandledList = new ArrayList<>();
        if (!Strings.isNullOrEmpty(personnelAlreadyHandled)) {
            personnelAlreadyHandledList = Arrays.asList(personnelAlreadyHandled.split(","));
        }

        com.aliyun.dingtalktodo_1_0.models.UpdateTodoTaskRequest updateTodoTaskRequest = new com.aliyun.dingtalktodo_1_0.models.UpdateTodoTaskRequest()
                .setExecutorIds(toDoPersonnelList)
                .setParticipantIds(personnelAlreadyHandledList);
        try {
            DynaBean dynaBean = metaService.selectOne("JE_MESSAGE_DINGTALK_TODO",
                    ConditionsWrapper.builder().eq("BUSINESS_KEY", pkValue).eq("TODO_JECLOUDDINGID", jeCloudId));
            client.updateTodoTaskWithOptions(unionId, dynaBean.getStr("TODO_DING_TASK_ID"),
                    updateTodoTaskRequest, updateTodoTaskHeaders, new com.aliyun.teautil.models.RuntimeOptions());
        } catch (TeaException err) {
            if (!com.aliyun.teautil.Common.empty(err.code) && !com.aliyun.teautil.Common.empty(err.message)) {
                // err 中含有 code 和 message 属性，可帮助开发定位问题
            }

        } catch (Exception _err) {
            TeaException err = new TeaException(_err.getMessage(), _err);
            if (!com.aliyun.teautil.Common.empty(err.code) && !com.aliyun.teautil.Common.empty(err.message)) {
                // err 中含有 code 和 message 属性，可帮助开发定位问题
            }

        }
    }

    @Override
    public void completeTodo(String accessToken, String pkValue, String unionId, String jeCloudId) throws Exception {
        DynaBean dynaBean = metaService.selectOne("JE_MESSAGE_DINGTALK_TODO",
                ConditionsWrapper.builder().eq("BUSINESS_KEY", pkValue).eq("TODO_JECLOUDDINGID", jeCloudId));
        String taskId = dynaBean.getStr("TODO_DING_TASK_ID");

        com.aliyun.dingtalktodo_1_0.Client client = DingTalkTaskServiceImpl.createClient();
        UpdateTodoTaskHeaders updateTodoTaskHeaders = new UpdateTodoTaskHeaders();
        updateTodoTaskHeaders.xAcsDingtalkAccessToken = accessToken;
        UpdateTodoTaskRequest updateTodoTaskRequest = new UpdateTodoTaskRequest()
                .setDone(true);
        try {
            UpdateTodoTaskResponse updateTodoTaskResponse = client.updateTodoTaskWithOptions(
                    unionId, taskId, updateTodoTaskRequest, updateTodoTaskHeaders, new RuntimeOptions());
            logger.info(JSON.toJSONString(updateTodoTaskResponse.getBody()));
        } catch (TeaException err) {
            if (!com.aliyun.teautil.Common.empty(err.code) && !com.aliyun.teautil.Common.empty(err.message)) {
                // err 中含有 code 和 message 属性，可帮助开发定位问题
                logger.info(err.code);
                logger.info(err.message);
            }
        } catch (Exception _err) {
            TeaException err = new TeaException(_err.getMessage(), _err);
            if (!com.aliyun.teautil.Common.empty(err.code) && !com.aliyun.teautil.Common.empty(err.message)) {
                // err 中含有 code 和 message 属性，可帮助开发定位问题
                logger.info(err.code);
                logger.info(err.message);
            }
        }
    }

    @Override
    public JSONObject getTodoInfo(String taskId, String accessToken, String userId, String jeCloudId) throws Exception {
        com.aliyun.dingtalktodo_1_0.Client client = DingTalkTaskServiceImpl.createClient();
        GetTodoTaskHeaders getTodoTaskHeaders = new GetTodoTaskHeaders();
        getTodoTaskHeaders.xAcsDingtalkAccessToken = accessToken;
        try {
            GetTodoTaskResponse todoTaskWithOptions = client.getTodoTaskWithOptions(userId,
                    taskId, getTodoTaskHeaders, new RuntimeOptions());
            logger.info(JSON.toJSONString(todoTaskWithOptions.getBody()));
            return JSONObject.parseObject(JSON.toJSONString(todoTaskWithOptions.getBody()));
        } catch (TeaException err) {
            if (!com.aliyun.teautil.Common.empty(err.code) && !com.aliyun.teautil.Common.empty(err.message)) {
                // err 中含有 code 和 message 属性，可帮助开发定位问题
                logger.info(err.code);
                logger.info(err.message);
            }
        } catch (Exception _err) {
            TeaException err = new TeaException(_err.getMessage(), _err);
            if (!com.aliyun.teautil.Common.empty(err.code) && !com.aliyun.teautil.Common.empty(err.message)) {
                // err 中含有 code 和 message 属性，可帮助开发定位问题
                logger.info(err.code);
                logger.info(err.message);
            }
        }
        return null;
    }

    @Override
    public void getOrgTodoList(String accessToken) throws Exception {
        com.aliyun.dingtalktodo_1_0.Client client = DingTalkTaskServiceImpl.createClient();
        QueryOrgTodoTasksHeaders queryOrgTodoTasksHeaders = new QueryOrgTodoTasksHeaders();
        queryOrgTodoTasksHeaders.xAcsDingtalkAccessToken = accessToken;
        QueryOrgTodoTasksRequest queryOrgTodoTasksRequest = new QueryOrgTodoTasksRequest()
                .setNextToken("0")
                .setIsDone(true);
        try {
            QueryOrgTodoTasksResponse queryOrgTodoTasksResponse = client.queryOrgTodoTasksWithOptions("tXg************RAiEiE", queryOrgTodoTasksRequest, queryOrgTodoTasksHeaders, new RuntimeOptions());
            logger.info(JSON.toJSONString(queryOrgTodoTasksResponse.getBody()));
        } catch (TeaException err) {
            if (!com.aliyun.teautil.Common.empty(err.code) && !com.aliyun.teautil.Common.empty(err.message)) {
                // err 中含有 code 和 message 属性，可帮助开发定位问题
                logger.info(err.code);
                logger.info(err.message);
            }
        } catch (Exception _err) {
            TeaException err = new TeaException(_err.getMessage(), _err);
            if (!com.aliyun.teautil.Common.empty(err.code) && !com.aliyun.teautil.Common.empty(err.message)) {
                // err 中含有 code 和 message 属性，可帮助开发定位问题
                logger.info(err.code);
                logger.info(err.message);
            }
        }
    }

    @Override
    public void deleteTodo(String accessToken, String userId, String pkValue, String jeCloudId) throws Exception {
        com.aliyun.dingtalktodo_1_0.Client client = DingTalkTaskServiceImpl.createClient();
        DeleteTodoTaskHeaders deleteTodoTaskHeaders = new DeleteTodoTaskHeaders();
        deleteTodoTaskHeaders.xAcsDingtalkAccessToken = accessToken;
        DeleteTodoTaskRequest deleteTodoTaskRequest = new DeleteTodoTaskRequest();
        try {
            DynaBean dynaBean = metaService.selectOne("JE_MESSAGE_DINGTALK_TODO",
                    ConditionsWrapper.builder().eq("BUSINESS_KEY", pkValue)
                            .eq("TODO_JECLOUDDINGID", jeCloudId)
            );
            if (dynaBean == null) {
                return;
            }
            String taskId = dynaBean.getStr("TODO_DING_TASK_ID");
            DeleteTodoTaskResponse deleteTodoTaskResponse = client.deleteTodoTaskWithOptions(userId, taskId, deleteTodoTaskRequest, deleteTodoTaskHeaders, new RuntimeOptions());
            logger.info(JSON.toJSONString(deleteTodoTaskResponse.getBody()));
            metaService.delete("JE_MESSAGE_DINGTALK_TODO", ConditionsWrapper.builder().eq("BUSINESS_KEY", pkValue)
                    .eq("TODO_JECLOUDDINGID", jeCloudId)
            );
        } catch (TeaException err) {
            if (!com.aliyun.teautil.Common.empty(err.code) && !com.aliyun.teautil.Common.empty(err.message)) {
                // err 中含有 code 和 message 属性，可帮助开发定位问题
                logger.info(err.code);
                logger.info(err.message);
            }
        } catch (Exception _err) {
            TeaException err = new TeaException(_err.getMessage(), _err);
            if (!com.aliyun.teautil.Common.empty(err.code) && !com.aliyun.teautil.Common.empty(err.message)) {
                // err 中含有 code 和 message 属性，可帮助开发定位问题
                logger.info(err.code);
                logger.info(err.message);
            }
        }
    }


}