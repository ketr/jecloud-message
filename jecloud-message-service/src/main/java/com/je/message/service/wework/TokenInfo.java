package com.je.message.service.wework;

public class TokenInfo {

    private final String token;

    private final int expiresIn;

    private final long createTime;

    public TokenInfo(String token, int expiresIn) {
        this.token = token;
        this.expiresIn = expiresIn;
        this.createTime = System.currentTimeMillis() / 1000; // 记录 token 创建时间（单位：秒）
    }

    public String getToken() {
        return token;
    }

    public int getExpiresIn() {
        return expiresIn;
    }

    public long getCreateTime() {
        return createTime;
    }
}

