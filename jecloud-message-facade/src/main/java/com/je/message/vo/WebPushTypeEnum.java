/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.message.vo;


public enum WebPushTypeEnum {
    /**
     * MSG推送到通知
     */
    MSG("MSG", "MSG"),

    /**
     * 微邮 推送到微邮，通知
     */
    MICROMAIL("MSG,MICROMAIL", "MICROMAIL"),

    /**
     * 流程 推送到流程
     */
    WF("WF", "WF"),


    /**
     * 批注 推送到通知，批注
     */
    POSTIL("MSG,POSTIL", "POSTIL");

    /**
     * 消息推送范围
     */
    private String code;

    /**
     *类型
     */
    private String name;

    WebPushTypeEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }


    /**
     * 检测是否存在
     *
     * @param name 编码
     * @return com.je.push.Push
     */
    public static WebPushTypeEnum check(String name) {

        //校验是否存在
        for (WebPushTypeEnum value : WebPushTypeEnum.values()) {
            if (value.getName().equals(name)) {
                return value;
            }
        }
        return null;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }
}
