package com.je.message.vo.dingTalk;

import java.util.List;

/**
 * 卡片消息
 */
public class DingTalkActionCardWorkNotice extends AbstractDingTalkWorkNotice {
    private String content;
    private String title;
    /**
     * 使用独立跳转ActionCard样式时的按钮排列方式：
     * <p>
     * 0：竖直排列
     * <p>
     * 1：横向排列
     * <p>
     * 必须与btnInfoList同时设置。
     */
    private String btnOrientation = "0";
    private List<BtnInfo> btnInfoList;

    private DingTalkActionCardWorkNotice() {
        super(DingTalkMsgTypeEnum.ACTION_CARD, "");
    }

    public DingTalkActionCardWorkNotice(String pkValue) {
        super(DingTalkMsgTypeEnum.ACTION_CARD, pkValue);
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }


    public static class BtnInfo {
        private String title;
        private String url;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBtnOrientation() {
        return btnOrientation;
    }

    public void setBtnOrientation(String btnOrientation) {
        this.btnOrientation = btnOrientation;
    }

    public List<BtnInfo> getBtnInfoList() {
        return btnInfoList;
    }

    public void setBtnInfoList(List<BtnInfo> btnInfoList) {
        this.btnInfoList = btnInfoList;
    }

}
