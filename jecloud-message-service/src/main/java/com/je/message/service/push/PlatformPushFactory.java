/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.message.service.push;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import com.je.common.base.service.rpc.SystemSettingRpcService;
import com.je.common.base.spring.SpringContextHolder;
import com.je.message.Push;
import org.apache.commons.lang.StringUtils;

/**
 * 获取对应平台的推送实例
 *
 * @author wangmm@ketr.com.cn
 * @date 2020/5/9
 */
public class PlatformPushFactory {

    /**
     * 根据平台获取实例
     *
     * @param platform 名称
     * @return com.je.push.PlatformPushService
     */
    public static PlatformPushService build(String platform) {

        //校验
        if (StringUtils.isBlank(platform)) {
            return null;
        }

        JSONObject config = new JSONObject();
        SystemSettingRpcService metaSystemSettingRpcService = SpringContextHolder.getBean(SystemSettingRpcService.class);
        //获取不同平台实例配置
        String platformConfig = metaSystemSettingRpcService.findSettingValue("JE_PUSH_CONFIG");
        if (StringUtils.isNotBlank(platformConfig)) {
            config = JSON.parseObject(platformConfig);
        }
        try {
            //获取枚举
            Push push = Push.check(platform);
            if (push != null) {
                //获取实例
                return build(push);
            } else {
                //TODO JE_PUSH_CONFIG 表的数据哪里来的？？？
                //获取配置的实例名称
                String beanName = config.getString(platform);
                //获取实例
                return SpringContextHolder.getBean(beanName);
            }

        } catch (Exception e) {
            return null;
        }
    }

    public static PlatformPushService build(Push push) {
        //校验
        if (push == null) {
            return null;
        }
        try {
            //获取实例名称
            String beanName = push.getBeanName();

            //获取实例
            return SpringContextHolder.getBean(beanName);
        } catch (Exception e) {
            return null;
        }
    }

}