/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.message.service.push.impl;

import com.je.common.base.message.vo.PushSystemMessage;
import com.je.common.base.util.SecurityUserHolder;
import com.je.message.rpc.SocketPushMessageRpcService;
import com.je.message.vo.Message;
import com.je.message.service.push.AbstractPlatformPushService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

/**
 * PlatformDwrPushServiceImpl
 *
 * @author wangmm@ketr.com.cn
 * @date 2020/5/09
 */
@Service("platformPushDWR")
public class PlatformDwrPushServiceImpl extends AbstractPlatformPushService {
    @Autowired
    private SocketPushMessageRpcService socketPushMessageRpcService;

    @Override
    public boolean send(Message message) {
        try {
            //变量替换
            format(message);
            //构建消息对象
            PushSystemMessage pushSystemMessage = new PushSystemMessage("",message.getContent());
            pushSystemMessage.setSourceUserId(SecurityUserHolder.getCurrentAccountRealUserId());
            List<String> targetUserIdList = new ArrayList<>();
            targetUserIdList.add(message.getUserId());
            pushSystemMessage.setTargetUserIds(targetUserIdList);

//            PushMsg msgVo = new PushMsg(message.getUserId(), message.getDeptId(), message.getTitle(), message.getContent());
//            msgVo.setLoginHistory(message.getLoginHistory());
//            msgVo.setLogin(message.getLogin());
//            msgVo.setBean(message.getBean());
//            msgVo.setWsConfig(message.getParams());
//            msgVo.setAddMsgList(message.isAddMsgList());
//            msgVo.setSignRemind(message.isSignRemind());
//            msgVo.setWebPushTypeEnum(message.getWebPushTypeEnum());
            //调用IM推送
            socketPushMessageRpcService.sendSystemMessage(pushSystemMessage);
            return true;
        } catch (Exception e) {
            logger.error(" PC 消息发送失败！[{}]{{}}", message.getTitle()
                    , message.getContent().length() > 100 ? message.getContent().substring(0, 100) : message.getContent());
            e.printStackTrace();
            return false;
        }
    }
}