/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.message.service.email.impl;

import cn.hutool.core.io.FileUtil;
import com.je.common.base.constants.message.SendContextType;
import com.je.common.base.exception.PlatformException;
import com.je.common.base.exception.PlatformExceptionEnum;
import com.je.common.base.service.rpc.DocumentInternalRpcService;
import com.je.common.base.service.rpc.SystemSettingRpcService;
import com.je.common.base.util.MessageUtils;
import com.je.common.base.util.StringUtil;
import com.je.message.exception.MessageSendException;
import com.je.message.model.MyAuthenticator;
import com.je.message.service.email.EmailOperationService;
import com.je.message.vo.EmailMsg;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.activation.DataHandler;
import javax.mail.*;
import javax.mail.internet.*;
import javax.mail.util.ByteArrayDataSource;
import java.io.File;
import java.io.FileNotFoundException;
import java.nio.file.Files;
import java.util.Date;
import java.util.List;
import java.util.Properties;

@Service
public class EmailOperationServiceImpl implements EmailOperationService {

    private static final Logger logger = LoggerFactory.getLogger(EmailOperationServiceImpl.class);

    @Autowired
    private DocumentInternalRpcService documentInternalRpcService;

    @Autowired
    private SystemSettingRpcService systemSettingRpcService;

    private static final String EMAIL = "EMAIL";
    private static final String USERNAME = "JE_SYS_EMAIL_USERNAME";
    private static final String PASSWORD = "JE_SYS_EMAIL_PASSWORD";
    private static final String SERVERVALIDATE = "JE_SYS_EMAIL_SERVERVALIDATE";
    private static final String EMAILSSL = "JE_SYS_EMAIL_SSL";
    private static final String SERVERHOST = "JE_SYS_EMAIL_SERVERHOST";
    private static final String SERVERPORT = "JE_SYS_EMAIL_SERVERPORT";
    private static final String SYSTITLE = "JE_SYS_EMAIL_SENDTITLE";
    private static final String PUSH_PLATFORM = "JE_PUSH_PLATFORM";
    private static final String MESSAGE_INTERFACE_TYPE_MISMATCH = "message.interface.type.mismatch";
    private static final String MESSAGE_ACCOUNT_OR_PASSWORD_NOT_EMPTY = "message.account.or.password.notEmpty";
    private static final String CHARSET_UTF_8 =  "UTF-8";

    @Override
    public void sendEmail(EmailMsg msgVo) throws Exception {
        //获取配置信息
        Properties properties = buildEmailConfig();
        //创建认证信息
        MyAuthenticator authenticator = createAuthenticator();
        Session sendMailSession = Session.getInstance(properties, authenticator);
        sendMailSession.setDebug(true);
        //创建email信息
        Message mailMessage = createEmailMessage(sendMailSession, msgVo);
        Transport.send(mailMessage);
    }

    private Message createEmailMessage(Session session, EmailMsg msgVo) throws Exception {
        Message message = new MimeMessage(session);

        String title = systemSettingRpcService.findSettingValue(SYSTITLE);
        String formAddress = systemSettingRpcService.findSettingValue(USERNAME);
        InternetAddress from = new InternetAddress(formAddress);
        if (StringUtil.isNotEmpty(title)) {
            from.setPersonal(title);
        }
        message.setFrom(from);
        //设置接收人信息
        setRecipients(message, msgVo);
        setHeaders(message, msgVo);
        //设置内容
        setMessageContent(message, msgVo);
        return message;
    }

    private void setRecipients(Message message, EmailMsg msgVo) throws MessagingException {
        if (msgVo.getReceiveEmail().split(",").length > 1) {
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(msgVo.getReceiveEmail()));
        } else {
            message.setRecipient(Message.RecipientType.TO, new InternetAddress(msgVo.getReceiveEmail()));
        }

        if (StringUtil.isNotEmpty(msgVo.getCs())) {
            message.setRecipients(Message.RecipientType.CC, InternetAddress.parse(msgVo.getCs()));
        }

        if (StringUtil.isNotEmpty(msgVo.getMs())) {
            message.setRecipients(Message.RecipientType.BCC, InternetAddress.parse(msgVo.getMs()));
        }
    }

    private void setHeaders(Message message, EmailMsg msgVo) throws MessagingException {
        message.setHeader("X-Priority", "1".equals(msgVo.getFaster()) ? "1" : "3");
        if ("1".equals(msgVo.getReplySign())) {
            message.setHeader("Disposition-Notification-To", systemSettingRpcService.findSettingValue(USERNAME));
        }
        message.setSubject(msgVo.getSubject());
        message.setSentDate(new Date());
    }

    private void setMessageContent(Message message, EmailMsg msgVo) throws Exception {
        String contextType = SendContextType.TEXT.equals(msgVo.getContextType()) ? "text/plain" : "text/html";
        Multipart mainPart = new MimeMultipart();

        // 创建一个包含邮件内容的 MimeBodyPart
        MimeBodyPart contentPart = new MimeBodyPart();
        contentPart.setContent(msgVo.getContext(), contextType + "; charset=" + CHARSET_UTF_8);
        mainPart.addBodyPart(contentPart);

        // 获取附件文件名和地址
        List<String> fileNames = msgVo.getFileNames();
        List<String> fileAddresses = msgVo.getAddresses();
        // 检查文件名和地址是否一致
        if (fileNames.size() != fileAddresses.size()) {
            throw new IllegalArgumentException("File names and addresses size mismatch.");
        }

        // 遍历所有文件并添加为附件
        for (int i = 0; i < fileNames.size(); i++) {
            try {
                File file = documentInternalRpcService.readFile(fileAddresses.get(i));
                if (file == null || !file.exists()) {
                    throw new FileNotFoundException("File not found: " + fileAddresses.get(i));
                }

                // 确保返回的 MIME 类型正确
                String mimeType = Files.probeContentType(file.toPath());
                if (mimeType == null) {
                    mimeType = "application/octet-stream";
                }

                ByteArrayDataSource source = new ByteArrayDataSource(FileUtil.getInputStream(file), mimeType);
                MimeBodyPart filePart = new MimeBodyPart();
                filePart.setDataHandler(new DataHandler(source));
                filePart.setFileName(MimeUtility.encodeText(fileNames.get(i), CHARSET_UTF_8, null));
                mainPart.addBodyPart(filePart);
            } catch (Exception e) {
                logger.error("Error adding file attachment: " + fileNames.get(i), e);
                throw new MessagingException("Error adding file attachment: " + fileNames.get(i), e);
            }
        }

        // 将 Multipart 对象设置为邮件内容
        message.setContent(mainPart);
    }

    private MyAuthenticator createAuthenticator() throws Exception {
        if (Boolean.parseBoolean(systemSettingRpcService.findSettingValue(SERVERVALIDATE))) {
            String userName = systemSettingRpcService.findSettingValue(USERNAME);
            String password = systemSettingRpcService.findSettingValue(PASSWORD);

            if (StringUtil.isEmpty(userName) || StringUtil.isEmpty(password)) {
                throw new PlatformException(
                        MessageUtils.getMessage(MESSAGE_ACCOUNT_OR_PASSWORD_NOT_EMPTY),
                        PlatformExceptionEnum.JE_MESSAGE_EMAIL_NOUSERNAME_ERROR,
                        new Object[]{}
                );
            }
            return new MyAuthenticator(userName, password);
        }
        return null;
    }

    private Properties buildEmailConfig() {

        String host = systemSettingRpcService.findSettingValue(SERVERHOST);
        String port = systemSettingRpcService.findSettingValue(SERVERPORT);

        if (!systemSettingRpcService.findSettingValue(PUSH_PLATFORM).contains(EMAIL)) {
            throw new MessageSendException(MessageUtils.getMessage(MESSAGE_INTERFACE_TYPE_MISMATCH));
        }

        Properties properties = new Properties();
        properties.put("mail.transport.protocol", "smtp");
        properties.put("mail.smtp.auth", Boolean.parseBoolean(systemSettingRpcService.findSettingValue(SERVERVALIDATE)) ? "true" : "false");

        if (Boolean.parseBoolean(systemSettingRpcService.findSettingValue(EMAILSSL))) {
            properties.put("mail.smtp.ssl.enable", true);
            properties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        }

        properties.put("mail.smtp.socketFactory.fallback", "false");
        properties.put("mail.smtp.host", host);
        properties.put("mail.smtp.port", port);
        properties.put("mail.smtp.socketFactory.port", port);
        properties.put("mail.smtp.connectiontimeout", "25000");
        properties.put("mail.smtp.timeout", "25000");

        return properties;
    }
}
