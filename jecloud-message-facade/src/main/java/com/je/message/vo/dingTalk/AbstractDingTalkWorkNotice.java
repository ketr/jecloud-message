package com.je.message.vo.dingTalk;

public class AbstractDingTalkWorkNotice {

    public DingTalkMsgTypeEnum type;
    public String pkValue;

    public AbstractDingTalkWorkNotice(DingTalkMsgTypeEnum type, String pkValue) {
        this.type = type;
        this.pkValue = pkValue;
    }

    public DingTalkMsgTypeEnum getType() {
        return type;
    }

    public void setType(DingTalkMsgTypeEnum type) {
        this.type = type;
    }

    public String getPkValue() {
        return pkValue;
    }

    public void setPkValue(String pkValue) {
        this.pkValue = pkValue;
    }


}
