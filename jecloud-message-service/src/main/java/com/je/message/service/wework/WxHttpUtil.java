package com.je.message.service.wework;

import cn.hutool.http.HttpUtil;
import com.google.common.base.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.nio.charset.StandardCharsets;

public class WxHttpUtil {

    private static final Logger logger = LoggerFactory.getLogger(WxHttpUtil.class);

    private static String get(String url) {
        String result = HttpUtil.get(url);
        return result;
    }

    private static String post(String url, String body) {
        String result = HttpUtil.post(url, body);
        return result;
    }

    public static String get(String url, String proxyHost, String proxyPort) {
        if (Strings.isNullOrEmpty(proxyHost)) {
            return get(url);
        }
        return gostGet(proxyHost, Integer.parseInt(proxyPort), url);
    }

    public static String post(String url, String body, String proxyHost, String proxyPort) {
        if (Strings.isNullOrEmpty(proxyHost)) {
            return post(url, body);
        }
        return gostPost(proxyHost, Integer.parseInt(proxyPort), url, body);
    }


    public static String gostGet(String proxyHost, int proxyPort, String targetUrl) {
        // 创建代理对象
        logger.info("----------------------------发送gost请求：");
        logger.info("----------------------------请求地址：" + proxyHost + proxyPort);
        logger.info("----------------------------目标地址：" + targetUrl);
        Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(proxyHost, proxyPort));
        try {
            // 创建 URL 对象
            URL url = new URL(targetUrl);
            // 创建 HTTP 连接，并设置代理
            HttpURLConnection conn = (HttpURLConnection) url.openConnection(proxy);
            // 设置请求方法
            conn.setRequestMethod("GET");
            // 发送请求
            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String inputLine;
            StringBuilder response = new StringBuilder();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            // 打印响应内容
            logger.info("----------------------------返回值信息：" + response.toString());
            return response.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }


    public static String gostPost(String proxyHost, int proxyPort, String targetUrl, String postData) {
        logger.info("----------------------------发送gost请求：");
        logger.info("----------------------------请求地址：" + proxyHost + proxyPort);
        logger.info("----------------------------目标地址：" + targetUrl);
        // 创建代理对象
        Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(proxyHost, proxyPort));
        try {
            // 创建 URL 对象
            URL url = new URL(targetUrl);

            // 创建 HTTP 连接，并设置代理
            HttpURLConnection conn = (HttpURLConnection) url.openConnection(proxy);

            // 设置请求方法为 POST
            conn.setRequestMethod("POST");

            // 启用输出流
            conn.setDoOutput(true);

            // 设置请求头
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

            // 将 POST 数据写入请求体
            try (DataOutputStream wr = new DataOutputStream(conn.getOutputStream())) {
                wr.write(postData.getBytes(StandardCharsets.UTF_8));
            }

            // 发送请求
            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String inputLine;
            StringBuilder response = new StringBuilder();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            // 打印响应内容
            logger.info("----------------------------返回值信息：" + response.toString());
            return response.toString();

        } catch (IOException e) {
            e.printStackTrace();
        }

        return "";
    }

}
