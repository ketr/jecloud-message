/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.message.controller.notification;

import com.google.common.collect.Lists;
import com.je.common.base.message.vo.Notice;
import com.je.common.base.message.vo.PushNoticeMessage;
import com.je.common.base.message.vo.PushScriptMessage;
import com.je.common.base.message.vo.PushSystemMessage;
import com.je.common.base.result.BaseRespResult;
import com.je.common.base.util.SecurityUserHolder;
import com.je.connector.rpc.ConnectionRegistryRpcService;
import com.je.message.rpc.SocketPushMessageRpcService;
import com.je.servicecomb.CommonRequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/je/message/socket")
public class SocketPushController implements CommonRequestService {

    @Autowired
    private SocketPushMessageRpcService socketPushMessageRpcService;
    @Autowired
    private ConnectionRegistryRpcService connectionRegistryRpcService;
    @RequestMapping(value = "/testSystem", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult testSystem(String userId,String busType,String content) {
        PushSystemMessage pushSystemMessage = new PushSystemMessage(busType,content);
        pushSystemMessage.setTargetUserIds(Lists.newArrayList(userId));
        pushSystemMessage.setSourceUserId(SecurityUserHolder.getCurrentAccountId());
        socketPushMessageRpcService.sendMessage(pushSystemMessage);
        return BaseRespResult.successResult("发送成功！");
    }

    @RequestMapping(value = "/testScript", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult testScript(String userId,String busType,String content) {
        PushScriptMessage pushScriptMessage = new PushScriptMessage(busType,content);
        pushScriptMessage.setTargetUserIds(Lists.newArrayList(userId));
        pushScriptMessage.setSourceUserId(SecurityUserHolder.getCurrentAccountId());
        socketPushMessageRpcService.sendMessage(pushScriptMessage);
        return BaseRespResult.successResult("发送成功！");
    }

    @RequestMapping(value = "/testOpenFuncGridScript", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult testOpenFuncGridScript(String userId,String funcCode,String busType,String content) {
        socketPushMessageRpcService.sendScriptOpenFuncGridMsgToUser(userId,funcCode,busType,content);
        return BaseRespResult.successResult("发送成功！");
    }

    @RequestMapping(value = "/testOpenFuncFormScript", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult testOpenFuncFormScript(String userId,String funcCode,String beanId,String busType,String content) {
        socketPushMessageRpcService.sendScriptOpenFuncFormMsgToUser(userId,funcCode,beanId,busType,content);
        return BaseRespResult.successResult("发送成功！");
    }

    @RequestMapping(value = "/testNotice", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult testNotice(String userId,String busType,String title,String content) {
        Notice notice = new Notice();
        notice.setTitle(title);
        notice.setContent(content);
        notice.setPlayAudio(true);
        PushNoticeMessage pushNoticeMessage = new PushNoticeMessage(busType,notice);
        pushNoticeMessage.setTargetUserIds(Lists.newArrayList(userId));
        pushNoticeMessage.setSourceUserId(SecurityUserHolder.getCurrentAccountId());
        socketPushMessageRpcService.sendMessage(pushNoticeMessage);
        return BaseRespResult.successResult("发送成功！");
    }

    @RequestMapping(value = "/testOpenFuncGridNotice", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult testOpenFuncGridNotice(String userId,String funcCode,String busType,String title,String content) {
        Notice notice = new Notice();
        notice.setTitle(title);
        notice.setContent(content);
        socketPushMessageRpcService.sendNoticeOpenFuncGridMsgToUser(userId,funcCode,busType,notice);
        return BaseRespResult.successResult("发送成功！");
    }

    @RequestMapping(value = "/testOpenFuncFormNotice", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult testOpenFuncFormNotice(String userId,String funcCode,String beanId,String busType,String title,String content) {
        Notice notice = new Notice();
        notice.setTitle(title);
        notice.setContent(content);
        socketPushMessageRpcService.sendNoticeOpenFuncFormMsgToUser(userId,funcCode,busType,notice,beanId);
        return BaseRespResult.successResult("发送成功！");
    }
    @RequestMapping(value = "/testGetConnect", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult testGetConnect(String userId,String funcCode,String beanId,String busType,String title,String content) {
        connectionRegistryRpcService.getAllApps();
        return BaseRespResult.successResult("发送成功！");
    }
}
