/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.message.vo;

import java.util.Map;

/**
 * 短信消息VO
 *
 * @author zhangshuaipeng
 */
public class NoteMsg {
    /**
     * 手机号
     */
    private String phoneNumber;
    /**
     * 发送人
     */
    private String fromUser;
    /**
     * 发送人ID
     */
    private String fromUserId;
    /**
     * 接受人
     */
    private String toUser;
    /**
     * 接受人ID
     */
    private String toUserId;
    /**
     * 发送内容
     */
    private String context;
    /**
     * 分类内容
     */
    private String fl;
    /**
     * 验证玛
     */
    private String msgCode;
    /**
     * 公司ID
     */
    private String jtgsId;
    /**
     * 租户ID
     */
    private String zhId;
    /**
     * 费用
     */
    private String moneyZhId;
    /**
     * 费用
     */
    private String moneyJtgsId;

    private String signName;
    private String templateCode;
    private Map<String, String> params;
    private String serviceType;

    public NoteMsg() {
        super();
    }

    /**
     * 发送短信
     *
     * @param phoneNumber 手机号
     * @param fromUser    发送人
     * @param fromUserId
     * @param toUser      接收人
     * @param toUserId
     * @param context     内容
     */
    public NoteMsg(String phoneNumber, String fromUser, String fromUserId, String toUser, String toUserId, String context) {
        super();
        this.phoneNumber = phoneNumber;
        this.fromUser = fromUser;
        this.fromUserId = fromUserId;
        this.toUser = toUser;
        this.toUserId = toUserId;
        this.context = context;
    }

    /**
     * 发送短信
     *
     * @param phoneNumber 手机号
     * @param toUser      接收人
     * @param toUserId
     * @param context     内容
     */
    public NoteMsg(String phoneNumber, String toUser, String toUserId,
                   String context) {
        super();
        this.phoneNumber = phoneNumber;
        this.toUser = toUser;
        this.toUserId = toUserId;
        this.context = context;
    }

    /**
     * 发送短信
     *
     * @param phoneNumber 手机号
     * @param context     内容
     */
    public NoteMsg(String phoneNumber, String context) {
        super();
        this.phoneNumber = phoneNumber;
        this.context = context;
    }

    public NoteMsg(String phoneNumber, String context, String serviceType, String signName, String templateCode, Map<String, String> params) {
        super();
        this.phoneNumber = phoneNumber;
        this.context = context;
        this.signName = signName;
        this.templateCode = templateCode;
        this.params = params;
        this.serviceType = serviceType;
    }

    public String getMsgCode() {
        return msgCode;
    }

    public void setMsgCode(String msgCode) {
        this.msgCode = msgCode;
    }

    public String getFromUser() {
        return fromUser;
    }

    public void setFromUser(String fromUser) {
        this.fromUser = fromUser;
    }

    public String getFromUserId() {
        return fromUserId;
    }

    public void setFromUserId(String fromUserId) {
        this.fromUserId = fromUserId;
    }

    public String getToUser() {
        return toUser;
    }

    public void setToUser(String toUser) {
        this.toUser = toUser;
    }

    public String getToUserId() {
        return toUserId;
    }

    public void setToUserId(String toUserId) {
        this.toUserId = toUserId;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }

    public String getFl() {
        return fl;
    }

    public void setFl(String fl) {
        this.fl = fl;
    }

    public String getJtgsId() {
        return jtgsId;
    }

    public void setJtgsId(String jtgsId) {
        this.jtgsId = jtgsId;
    }

    public String getMoneyJtgsId() {
        return moneyJtgsId;
    }

    public void setMoneyJtgsId(String moneyJtgsId) {
        this.moneyJtgsId = moneyJtgsId;
    }

    public String getZhId() {
        return zhId;
    }

    public void setZhId(String zhId) {
        this.zhId = zhId;
    }

    public String getMoneyZhId() {
        return moneyZhId;
    }

    public void setMoneyZhId(String moneyZhId) {
        this.moneyZhId = moneyZhId;
    }

    public Map<String, String> getParams() {
        return params;
    }

    public void setParams(Map<String, String> params) {
        this.params = params;
    }

    public String getSignName() {
        return signName;
    }

    public void setSignName(String signName) {
        this.signName = signName;
    }

    public String getTemplateCode() {
        return templateCode;
    }

    public void setTemplateCode(String templateCode) {
        this.templateCode = templateCode;
    }
}
