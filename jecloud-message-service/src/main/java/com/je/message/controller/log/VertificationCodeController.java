/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.message.controller.log;

import com.je.auth.check.annotation.AuthCheckDataPermission;
import com.je.auth.check.annotation.CheckDataType;
import com.je.common.base.mvc.AbstractPlatformController;
import com.je.common.base.mvc.BaseMethodArgument;
import com.je.common.base.result.BaseRespResult;
import com.je.rbac.annotation.ControllerAuditLog;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(value = "/je/message/log/vertification")
public class VertificationCodeController extends AbstractPlatformController {

    @Override
    @RequestMapping(value = "/load", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ControllerAuditLog(moduleName = "验证码日志模块",operateTypeCode = "loadVertificationLog",operateTypeName = "查看验证码日志",logTypeCode = "systemManage",logTypeName = "系统管理")
    @AuthCheckDataPermission(checkDataType = CheckDataType.LOAD)
    public BaseRespResult load(BaseMethodArgument param, HttpServletRequest request) {
        return super.load(param, request);
    }

    @Override
    @RequestMapping(value = "/doSave", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @AuthCheckDataPermission(checkDataType = CheckDataType.UPDATE)
    @ControllerAuditLog(moduleName = "验证码日志模块",operateTypeCode = "saveVertificationLog",operateTypeName = "保存验证码日志",logTypeCode = "systemManage",logTypeName = "系统管理")
    public BaseRespResult doSave(BaseMethodArgument param, HttpServletRequest request) {
        return super.doSave(param, request);
    }

    @Override
    @RequestMapping(value = "/doUpdate", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @AuthCheckDataPermission(checkDataType = CheckDataType.UPDATE)
    @ControllerAuditLog(moduleName = "验证码日志模块",operateTypeCode = "updateVertificationLog",operateTypeName = "更新验证码日志",logTypeCode = "systemManage",logTypeName = "系统管理")
    public BaseRespResult doUpdate(BaseMethodArgument param, HttpServletRequest request) {
        return super.doUpdate(param, request);
    }

    @Override
    @RequestMapping(value = "/doRemove", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @AuthCheckDataPermission(checkDataType = CheckDataType.UPDATE)
    @ControllerAuditLog(moduleName = "验证码日志模块",operateTypeCode = "removeVertificationLog",operateTypeName = "移除验证码日志",logTypeCode = "systemManage",logTypeName = "系统管理")
    public BaseRespResult doRemove(BaseMethodArgument param, HttpServletRequest request) {
        return super.doRemove(param, request);
    }

    @Override
    @RequestMapping(value = "/doUpdateList", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @AuthCheckDataPermission(checkDataType = CheckDataType.UPDATE)
    @ControllerAuditLog(moduleName = "验证码日志模块",operateTypeCode = "updateVertificationLogList",operateTypeName = "更新验证码日志集合",logTypeCode = "systemManage",logTypeName = "系统管理")
    public BaseRespResult doUpdateList(BaseMethodArgument param, HttpServletRequest request) {
        return super.doUpdateList(param, request);
    }

}
