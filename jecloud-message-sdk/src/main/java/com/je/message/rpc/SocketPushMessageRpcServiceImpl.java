/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.message.rpc;

import com.je.common.base.message.vo.*;
import com.je.common.base.service.rpc.PushMessageRpcService;
import org.apache.servicecomb.provider.pojo.RpcReference;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class SocketPushMessageRpcServiceImpl implements SocketPushMessageRpcService, PushMessageRpcService {

    @RpcReference(microserviceName = "message", schemaId = "socketPushMessageRpcService")
    private SocketPushMessageRpcService socketPushMessageRpcService;

    @Override
    public void sendMessage(PushMessage message) {
        socketPushMessageRpcService.sendMessage(message);
    }

    @Override
    public void sendSystemMessage(PushSystemMessage message) {
        socketPushMessageRpcService.sendSystemMessage(message);
    }

    @Override
    public void sendScriptMessage(PushScriptMessage message) {
        socketPushMessageRpcService.sendScriptMessage(message);
    }

    @Override
    public void sendScriptOpenFuncGridMsgToUser(String userId, String funcCode, String busType, String content) {
        socketPushMessageRpcService.sendScriptOpenFuncGridMsgToUser(userId, funcCode, busType, content);
    }

    @Override
    public void sendScriptOpenFuncGridMsgToUsers(List<String> userIdList, String funcCode, String busType, String content) {
        socketPushMessageRpcService.sendScriptOpenFuncGridMsgToUsers(userIdList, funcCode, busType, content);
    }

    @Override
    public void sendScriptOpenFuncFormMsgToUser(String userId, String funcCode, String busType, String content, String beanId) {
        socketPushMessageRpcService.sendScriptOpenFuncFormMsgToUser(userId, funcCode, busType, content, beanId);
    }

    @Override
    public void sendScriptOpenFuncFormMsgToUsers(List<String> userIdList, String funcCode, String busType, String content, String beanId) {
        socketPushMessageRpcService.sendScriptOpenFuncFormMsgToUsers(userIdList, funcCode, busType, content, beanId);
    }

    @Override
    public void sendNoReadMessage(PushNoReadMessage message) {
        socketPushMessageRpcService.sendNoReadMessage(message);
    }

    @Override
    public void sendNoticeMessage(PushNoticeMessage message) {
        socketPushMessageRpcService.sendNoticeMessage(message);
    }

    @Override
    public void sendNoticeOpenFuncGridMsgToUser(String userId, String funcCode, String busType, Notice content) {
        socketPushMessageRpcService.sendNoticeOpenFuncGridMsgToUser(userId, funcCode, busType, content);
    }

    @Override
    public void sendNoticeOpenFuncGridMsgToUsers(List<String> userIdList, String funcCode, String busType, Notice content) {
        socketPushMessageRpcService.sendNoticeOpenFuncGridMsgToUsers(userIdList, funcCode, busType, content);
    }

    @Override
    public void sendNoticeOpenFuncFormMsgToUser(String userId, String funcCode, String busType, Notice content, String beanId) {
        socketPushMessageRpcService.sendNoticeOpenFuncFormMsgToUser(userId, funcCode, busType, content, beanId);
    }

    @Override
    public void sendNoticeOpenFuncFormMsgToUsers(List<String> userIdList, String funcCode, String busType, Notice content, String beanId) {
        socketPushMessageRpcService.sendNoticeOpenFuncFormMsgToUsers(userIdList, funcCode, busType, content, beanId);
    }

}
