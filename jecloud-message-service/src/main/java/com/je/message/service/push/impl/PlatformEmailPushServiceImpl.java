/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.message.service.push.impl;

import com.je.common.base.DynaBean;
import com.je.common.base.constants.message.SendContextType;
import com.je.common.base.mapper.query.NativeQuery;
import com.je.common.base.util.StringUtil;
import com.je.message.vo.Message;
import com.je.message.service.email.EmailService;
import com.je.message.service.push.AbstractPlatformPushService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 邮件
 *
 * @author wangmm@ketr.com.cn
 * @date 2020/5/09
 */
@Component("platformPushEmail")
public class PlatformEmailPushServiceImpl extends AbstractPlatformPushService {

    @Autowired
    private EmailService emailService;

    @Override
    public boolean send(Message message) {
        try {
            //变量替换
            format(message);

            //获取用户
            List<DynaBean> users = metaRbacService.selectByTableCodeAndNativeQuery("JE_RBAC_USER", NativeQuery.build().eq("JE_RBAC_USER_ID", message.getUserId()));

            //发送消息
            for (DynaBean user : users) {
                String email = user.getStr("USER_MAIL");
                if (StringUtil.isNotEmpty(email)) {
                    emailService.send(email, message.getTitle(), SendContextType.HTML, message.getContent());
                }
            }
            return true;
        } catch (Exception e) {
            logger.error(" 邮件 消息发送失败！[{}]{{}}", message.getTitle()
                    , message.getContent().length() > 100 ? message.getContent().substring(0, 100) : message.getContent());
            e.printStackTrace();
            return false;
        }
    }
}