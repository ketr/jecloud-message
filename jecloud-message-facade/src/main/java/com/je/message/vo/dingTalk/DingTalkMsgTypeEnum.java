package com.je.message.vo.dingTalk;

import java.util.HashMap;

public enum DingTalkMsgTypeEnum {

    TEXT("text"),

    ACTION_CARD("actionCard"),

    MARKDOWN("markdown");

    /**
     * 类型
     */
    private String type;

    DingTalkMsgTypeEnum(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    private static final HashMap<String, DingTalkMsgTypeEnum> map = new HashMap<>();

    static {
        map.put(DingTalkMsgTypeEnum.TEXT.getType(), DingTalkMsgTypeEnum.TEXT);
        map.put(DingTalkMsgTypeEnum.ACTION_CARD.getType(), DingTalkMsgTypeEnum.ACTION_CARD);
        map.put(DingTalkMsgTypeEnum.MARKDOWN.getType(), DingTalkMsgTypeEnum.MARKDOWN);
    }

    public static DingTalkMsgTypeEnum getMessage(String type) {
        return map.get(type);
    }
}
