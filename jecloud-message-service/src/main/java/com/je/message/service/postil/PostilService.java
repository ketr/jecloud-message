/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.message.service.postil;

import com.alibaba.fastjson2.JSONObject;
import com.je.common.base.DynaBean;
import com.je.message.vo.PostilVo;
import java.util.List;

/**
 * 批注服务定义
 */
public interface PostilService {

    /**
     * 保存批注信息
     *
     * @param dynaBean 动态类
     * @return
     */
    List<DynaBean> savePostil(DynaBean dynaBean);

    /**
     * 加载批注信息
     *
     * @param userId    用户id
     * @param keyWord   关键字
     * @param startDate 开始时间
     * @param endDate   结束时间
     * @return
     */
    List<PostilVo> loadPostils(String type, String userId, String keyWord, String startDate, String endDate, int start, int limitt, JSONObject returnObj);

    /**
     * 设置成全部已读
     *
     * @param keyWord
     */
    void readAllPostil(String keyWord);

}
