/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.message.controller.notification;


import cn.hutool.core.collection.CollUtil;
import com.alibaba.fastjson2.JSONObject;
import com.google.common.base.Strings;
import com.je.common.base.DynaBean;
import com.je.common.base.constants.dd.DDType;
import com.je.common.base.mapper.query.NativeQuery;
import com.je.common.base.mapper.query.Query;
import com.je.common.base.mvc.AbstractPlatformController;
import com.je.common.base.mvc.BaseMethodArgument;
import com.je.common.base.result.BaseRespResult;
import com.je.common.base.service.MetaResourceService;
import com.je.common.base.service.rpc.BeanService;
import com.je.common.base.util.SecurityUserHolder;
import com.je.common.base.util.StringUtil;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.ibatis.extension.plugins.pagination.Page;
import com.je.message.service.portal.PortalService;
import com.je.message.vo.WebPushTypeEnum;
import com.je.meta.model.dd.DictionaryItemVo;
import com.je.meta.rpc.dictionary.DictionaryRpcService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * 通知消息controller
 */
@RestController
@RequestMapping(value = "/je/message/homePortal")
public class HomeNotificationController extends AbstractPlatformController {

    private Logger logger = LoggerFactory.getLogger(HomeNotificationController.class);

    @Autowired
    private DictionaryRpcService dictionaryRpcService;
    @Autowired
    private MetaResourceService metaResourceService;
    @Autowired
    private PortalService portalService;

    /**
     * @param
     * @note 加载通知消息
     * @date
     */
    @RequestMapping(value = "/loadUserMsg", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public BaseRespResult loadUserMsg(BaseMethodArgument param, HttpServletRequest request) {
        //TODO 统计数量的消息类型
        String excludeTypes = "WF,PZ,POSTIL";
        String ddValueStr = "";
        Query query = new Query();
        DynaBean dictionary = metaResourceService.selectOne("JE_CORE_DICTIONARY", NativeQuery.build().eq("DICTIONARY_DDCODE", "JE_CORE_MSGTYPE")
                .eq("DICTIONARY_DDTYPE", DDType.LIST).apply("and (SY_STATUS = '' or SY_STATUS = '1')"), null);
        if (dictionary == null) {
            logger.error("数据字典【 JE_CORE_MSGTYPE  】未找到!");
            ddValueStr = "[]";
        } else {
            List<DictionaryItemVo> itemVoList = dictionaryRpcService.buildChildrenList(dictionary, false, query, "");
            ArrayList<String> list = CollUtil.newArrayList();
            for (DictionaryItemVo dictionaryItemVo : itemVoList) {
                list.add(dictionaryItemVo.getCode());
            }
            ddValueStr = String.join(",", list);
        }
        String baseSql = " AND USERMSG_MSGTYPE_CODE IN (" + StringUtil.buildArrayToString(ddValueStr.split(",")) + ")";
        DynaBean dynaBean = (DynaBean) request.getAttribute("dynaBean");
        String userId = getStringParameter(request, "userId");
        String deptId = getStringParameter(request, "deptId");
        String msgType = getStringParameter(request, "msgType");
        String readStatus = getStringParameter(request, "readStatus");
        String startTime = getStringParameter(request, "startTime");
        String endTime = getStringParameter(request, "endTime");
        String keyword = getStringParameter(request, "keyword");
        if (Strings.isNullOrEmpty(userId) || Strings.isNullOrEmpty(deptId)) {
            userId = SecurityUserHolder.getCurrentAccountRealUserId();
            deptId = SecurityUserHolder.getCurrentAccountRealOrgId();
        }
        ConditionsWrapper whereSqlWrapper = ConditionsWrapper.builder().table(dynaBean.getTableCode());
        ConditionsWrapper sqlWrapper = ConditionsWrapper.builder().apply("SELECT USERMSG_YD,COUNT(*) AS READNUM FROM JE_SYS_USERMSG WHERE ");
        ConditionsWrapper sqlWrapper2 = ConditionsWrapper.builder().apply("SELECT USERMSG_MSGTYPE_CODE,COUNT(*) AS TYPENUM FROM JE_SYS_USERMSG WHERE ");
        whereSqlWrapper.apply("USERMSG_JSR_ID= {0} AND USERMSG_JSRSZBM_ID = {1} ", userId, deptId);
        sqlWrapper.apply(" USERMSG_JSR_ID={0} AND USERMSG_JSRSZBM_ID ={1}", userId, deptId);
        sqlWrapper2.apply(" USERMSG_JSR_ID={0} AND USERMSG_JSRSZBM_ID ={1}", userId, deptId);
        if (StringUtil.isEmpty(msgType)) {
            whereSqlWrapper.apply(baseSql);
            sqlWrapper.apply(baseSql);
            sqlWrapper2.apply(baseSql);
        } else {
            whereSqlWrapper.apply(" AND USERMSG_MSGTYPE_CODE = {0}", msgType);
            sqlWrapper.apply(" AND USERMSG_MSGTYPE_CODE = {0} ", msgType);
            sqlWrapper2.apply(" AND USERMSG_MSGTYPE_CODE = {0} ", msgType);
        }
        if (StringUtil.isNotEmpty(readStatus)) {
            whereSqlWrapper.apply(" AND USERMSG_YD = {0}", readStatus);
        }
        if (StringUtil.isNotEmpty(startTime) && StringUtil.isNotEmpty(endTime)) {
            whereSqlWrapper.apply(" AND USERMSG_JSSJ > {0} AND USERMSG_JSSJ <  {1} ", startTime + " 00:00:00", endTime + " 23:59:59");
            sqlWrapper.apply(" AND USERMSG_JSSJ > {0} AND USERMSG_JSSJ <  {1} ", startTime + " 00:00:00", endTime + " 23:59:59");
            sqlWrapper2.apply(" AND USERMSG_JSSJ > {0} AND USERMSG_JSSJ <  {1} ", startTime + " 00:00:00", endTime + " 23:59:59");
        }
        if (StringUtil.isNotEmpty(keyword)) {
            String likeKeyWord = "%" + keyword + "%";
            whereSqlWrapper.apply(" AND (USERMSG_BT like {0}  OR SY_CREATEUSERNAME like {1} OR USERMSG_NR like {2})", likeKeyWord, likeKeyWord, likeKeyWord);
            sqlWrapper.apply(" AND (USERMSG_BT like {0}  OR SY_CREATEUSERNAME like {1} OR USERMSG_NR like {2})", likeKeyWord, likeKeyWord, likeKeyWord);
            sqlWrapper2.apply(" AND (USERMSG_BT like {0}  OR SY_CREATEUSERNAME like {1} OR USERMSG_NR like {2})", likeKeyWord, likeKeyWord, likeKeyWord);
        }
        String orderSql = getStringParameter(request, "orderSql");
        String groupSql = getStringParameter(request, "groupSql");
        if (StringUtil.isNotEmpty(orderSql)) {
            whereSqlWrapper.apply(orderSql);
        } else {
            whereSqlWrapper.apply("order by SY_CREATETIME desc");
        }
        if (StringUtil.isNotEmpty(groupSql)) {
            whereSqlWrapper.apply(groupSql);
        }

        int pageNum = param.getPage();
        int limit = param.getLimit();
        long count = 0L;
        int currentPage = 0;
        int pages = 0;
        List<DynaBean> list;
        if (limit == -1) {
            list = metaService.select(whereSqlWrapper);
            count = list.size();

        } else {
            Page page = new Page(pageNum, limit);
            list = metaService.select(dynaBean.getTableCode(), page, whereSqlWrapper);
            count = page.getTotal();
            currentPage = page.getCurrent();
            pages = page.getPages();
        }
        JSONObject returnObj = new JSONObject();
        //获取当前登陆人的已读数、未读数、总数
        sqlWrapper.apply(" GROUP BY USERMSG_YD");
        List<Map<String, Object>> countVals = metaService.selectSql(sqlWrapper);
        Long allCount = 0L;
        Long readCount = 0L;
        Long noReadCount = 0L;
        for (Map countVal : countVals) {
            String ydFlag = countVal.get("USERMSG_YD") + "";
            String readNumStr = StringUtil.getDefaultValue(countVal.get("READNUM"), "0");
            Long readNum = Long.parseLong(readNumStr);
            if ("1".equals(ydFlag)) {
                readCount += readNum;
            }
            allCount += readNum;
        }
        noReadCount = allCount - readCount;
        returnObj.put("allCount", allCount);
        returnObj.put("readCount", readCount);
        returnObj.put("noReadCount", noReadCount);
        //获取当前登陆人各类型数量
        List<Map<String, Object>> typeVals = metaService.selectSql(sqlWrapper2.apply(" GROUP BY USERMSG_MSGTYPE_CODE"));
        for (Map typeVal : typeVals) {
            String type = typeVal.get("USERMSG_MSGTYPE_CODE") + "";
            if (StringUtil.isEmpty(type)) continue;
            String readNumStr = StringUtil.getDefaultValue(typeVal.get("TYPENUM"), "0");
            Long readNum = Long.parseLong(readNumStr);
            returnObj.put(type, readNum);
        }
        List<HashMap> values = new ArrayList<>();
        for (DynaBean bean : list) {
            values.add(bean.getValues());
        }
        returnObj.put("rows", values);
        returnObj.put("totalCount", count);
        returnObj.put("currentPage", currentPage);
        returnObj.put("pages", pages);
        return BaseRespResult.successResult(returnObj);
    }

    /**
     * @param
     * @note 加载通知消息
     * @date
     */
    @RequestMapping(value = "/loadMsgByMsgId", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public BaseRespResult loadMsgByMsgId(HttpServletRequest request) {
        String msgId = getStringParameter(request, "msgId");
        if (Strings.isNullOrEmpty(msgId)) {
            return BaseRespResult.errorResult("参数错误！");
        }
        DynaBean dynaBean = metaService.selectOne("JE_SYS_USERMSG", ConditionsWrapper.builder().eq("JE_SYS_USERMSG_ID", msgId));
        return BaseRespResult.successResult(dynaBean);
    }

    /**
     * @note 搜索通知类型
     */
    @RequestMapping(value = "/loadMsgType", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public BaseRespResult loadMsgType(BaseMethodArgument param, HttpServletRequest request) {
        String keyWord = getStringParameter(request, "keyWord");
        Query query = new Query();
        DynaBean dictionary = metaResourceService.selectOne("JE_CORE_DICTIONARY", NativeQuery.build().eq("DICTIONARY_DDCODE", "JE_CORE_MSGTYPE")
                .eq("DICTIONARY_DDTYPE", DDType.LIST).apply("and (SY_STATUS = '' or SY_STATUS = '1')"), null);

        ArrayList<DictionaryItemVo> list = CollUtil.newArrayList();
        if (dictionary == null) {
            logger.error("数据字典【 JE_CORE_MSGTYPE  】未找到!");
            return BaseRespResult.errorResult("数据字典【 JE_CORE_MSGTYPE  】未找到!");
        } else {
            List<DictionaryItemVo> itemVoList = dictionaryRpcService.buildChildrenList(dictionary, false, query, "");
            for (DictionaryItemVo dictionaryItemVo : itemVoList) {
                if (dictionaryItemVo.getText().contains(keyWord)) {
                    list.add(dictionaryItemVo);
                }

            }
        }
        return BaseRespResult.successResult(list);
    }


    /**
     * @note 设置全部已读通知
     */
    @RequestMapping(value = "/readAllUserMsg", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public BaseRespResult readAllUserMsg(BaseMethodArgument param, HttpServletRequest request) {
        String userId = getStringParameter(request, "userId");
        String deptId = getStringParameter(request, "deptId");
        if (Strings.isNullOrEmpty(userId) || Strings.isNullOrEmpty(deptId)) {
            userId = SecurityUserHolder.getCurrentAccountRealUserId();
            deptId = SecurityUserHolder.getCurrentAccountRealOrgId();
        }
        //TODO 进行已读操作的消息类型
//        String excludeTypes = "WF,PZ,POSTIL";
        String ddValueStr = "";
        Query query = new Query();
        DynaBean dictionary = metaResourceService.selectOne("JE_CORE_DICTIONARY", NativeQuery.build()
                .eq("DICTIONARY_DDCODE", "JE_CORE_MSGTYPE").eq("DICTIONARY_DDTYPE", DDType.LIST)
                .apply("and (SY_STATUS = '' or SY_STATUS = '1')"), null);

        if (dictionary == null) {
            logger.error("数据字典【 JE_CORE_MSGTYPE  】未找到!");
            ddValueStr = "[]";
        } else {
            List<DictionaryItemVo> itemVoList = dictionaryRpcService.buildChildrenList(dictionary, false, query, "");
            ArrayList<String> list = CollUtil.newArrayList();
            for (DictionaryItemVo dictionaryItemVo : itemVoList) {
                list.add(dictionaryItemVo.getCode());
            }
            ddValueStr = String.join(",", list);
        }
        String baseSql = " AND USERMSG_MSGTYPE_CODE IN (" + StringUtil.buildArrayToString(ddValueStr.split(",")) + ")";
        metaService.executeSql(" UPDATE JE_SYS_USERMSG SET USERMSG_YD='1' WHERE USERMSG_JSR_ID='" + userId + "' AND USERMSG_JSRSZBM_ID ='" + deptId + "'" + baseSql);
        return BaseRespResult.successResult("设置成功！");
    }


    /**
     * @note 加载消息阅读标记
     */
    @RequestMapping(value = "/loadReadSign", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public BaseRespResult loadReadSign(BaseMethodArgument param, HttpServletRequest request) {
        String userId = getStringParameter(request, "userId");
        String deptId = getStringParameter(request, "deptId");
        if (Strings.isNullOrEmpty(userId) || Strings.isNullOrEmpty(deptId)) {
            userId = SecurityUserHolder.getCurrentAccountRealUserId();
            deptId = SecurityUserHolder.getCurrentAccountRealOrgId();
        }
        String pushType = getStringParameter(request, "pushType");

        return portalService.loadReadSign(userId, deptId, pushType);
    }

    /**
     * @note 修改消息阅读标记
     */
    @RequestMapping(value = "/doUpdateSign", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public BaseRespResult doUpdateSign(BaseMethodArgument param, HttpServletRequest request) {
        String userId = getStringParameter(request, "userId");
        String deptId = getStringParameter(request, "deptId");
        String pushType = getStringParameter(request, "pushType");
        String actionType = getStringParameter(request, "actionType");
        WebPushTypeEnum webPushTypeEnum = WebPushTypeEnum.check(pushType);
        if (webPushTypeEnum == null) {
            return BaseRespResult.errorResult("请确定类型是否正确！");
        }
        if (Strings.isNullOrEmpty(userId) || Strings.isNullOrEmpty(deptId)) {
            userId = SecurityUserHolder.getCurrentAccountRealUserId();
            deptId = SecurityUserHolder.getCurrentAccountRealOrgId();
        }
        return portalService.insertOrUpdateSign(userId, deptId, webPushTypeEnum, actionType);
    }

    /**
     * @note 修改消息阅读标记
     */
    @RequestMapping(value = "/doUpdateOneSign", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public BaseRespResult doUpdateOneSign(BaseMethodArgument param, HttpServletRequest request) {
        String pkValue = getStringParameter(request, "JE_SYS_USERMSG_ID");

        DynaBean dynaBean = metaService.selectOne("JE_SYS_USERMSG", ConditionsWrapper.builder().eq("JE_SYS_USERMSG_ID", pkValue));
        if (dynaBean != null) {
            dynaBean.setStr("USERMSG_YD", "1");
            metaService.update(dynaBean);
        }
        return BaseRespResult.successResult("修改成功");
    }

    /**
     * 获取通知消息角标
     *
     * @return
     */
    @RequestMapping(value = "/getNoteMsgNum", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public BaseRespResult getNoteMsgNum(BaseMethodArgument param, HttpServletRequest request) {
        String userId = getStringParameter(request, "userId");
        String deptId = getStringParameter(request, "deptId");
        if (Strings.isNullOrEmpty(userId) || Strings.isNullOrEmpty(deptId)) {
            userId = SecurityUserHolder.getCurrentAccountRealUserId();
            deptId = SecurityUserHolder.getCurrentAccountRealOrgId();
        }
        String sql = "";
        sql += "USERMSG_JSR_ID='" + userId + "' AND USERMSG_JSRSZBM_ID ='" + deptId + "'";
        List<Map<String, Object>> countVals = metaService.selectSql(" SELECT USERMSG_YD,COUNT(*) AS READNUM FROM JE_SYS_USERMSG WHERE " + sql + " GROUP BY USERMSG_YD");
        Long readNum = 0L;
        for (Map countVal : countVals) {
            if (countVal.containsKey("USERMSG_YD") && countVal.get("USERMSG_YD").toString().equals("0")) {
                String readNumStr = StringUtil.getDefaultValue(countVal.get("READNUM"), "0");
                readNum = Long.parseLong(readNumStr);
            }
        }
        return BaseRespResult.successResult(readNum);
    }
}