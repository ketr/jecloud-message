/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.message.service.portal;

import com.je.common.base.message.vo.NoticeMsg;
import com.je.common.base.portal.vo.PushActVo;
import com.je.common.base.result.BaseRespResult;
import com.je.message.vo.WebPushTypeEnum;

import java.util.List;

public interface PortalService {

    /**
     * 推送指定消息(未读数字、角标、列表)
     * @param userId 用户id
     * @param pushTypes 推送消息类型
     * @param pushActs 更新内容
     * @return
     */
    void push(String userId,String deptId, String[] pushTypes, String[] pushActs);

    /**
     * 获取推送消息(未读数字、角标、列表)
     * @param userId 用户id
     * @param pushTypes 推送消息类型
     * @param pushActs 更新内容
     * @return
     */
    List<PushActVo> getPushInfo(String userId, String[] pushTypes, String[] pushActs);

    /**
     * 根据消息类型 添加或删除 未读标记（首页右上角小红点）
     * @param userId 用户id
     * @param pushTypes 推送消息类型
     * @param actionType 操作类型 添加 删除
     * @return
     */
    BaseRespResult insertOrUpdateSign(String userId,String deptId, WebPushTypeEnum pushTypes , String actionType);

    /**
     * 根据消息类型 加载阅读 标记（首页右上角小红点）
     * @param userId 用户id
     * @param pushType 推送消息类型
     * @return
     */
    BaseRespResult loadReadSign(String userId,String deptId, String pushType);

    /**
     * 添加通知消息
     * @param noticeMsg 通知消息对象
     * @return
     */
    BaseRespResult insertNoticeMsg(NoticeMsg noticeMsg);
}
