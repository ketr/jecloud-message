/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.message.service.postil.impl;

import com.alibaba.fastjson2.JSONObject;
import com.je.common.base.portal.service.PostilPortalService;
import com.je.common.base.service.MetaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PostilPortalServiceImpl implements PostilPortalService {

    @Autowired
    private MetaService metaService;

    @Override
    public JSONObject getPostilNum(String userId) {
        JSONObject numObj=new JSONObject();
        StringBuffer whereSql=new StringBuffer();
        whereSql.append(" AND (SY_CREATEUSERID='"+userId+"' OR POSTIL_HFRID='"+userId+"') ");
        whereSql.append(" AND POSTIL_MODELID NOT IN (SELECT FUNCEDIT_PKVALUE FROM JE_CORE_FUNCEDIT WHERE FUNCEDIT_USERID='"+userId+"' AND FUNCEDIT_FUNCCODE='JE_CORE_POSTIL' AND FUNCEDIT_NEW='0')");
        String querySql=" SELECT POSTIL_FUNCCODE,POSTIL_TABLECODE,POSTIL_MODELID,COUNT(*) POSTILCOUNT,MAX(SY_CREATETIME) SY_CREATETIME FROM JE_CORE_POSTIL WHERE 1=1 "+whereSql.toString()+" GROUP BY POSTIL_FUNCCODE,POSTIL_TABLECODE,POSTIL_MODELID ";
        Long count = metaService.countBySql(querySql);
        numObj.put("numCount",count);
        return numObj;
    }

    @Override
    public JSONObject getPostilBadge(String userId) {
        JSONObject numObj=new JSONObject();
        StringBuffer whereSql=new StringBuffer();
        whereSql.append(" AND (SY_CREATEUSERID='"+userId+"' OR POSTIL_HFRID='"+userId+"') ");
        whereSql.append(" AND POSTIL_MODELID NOT IN (SELECT FUNCEDIT_PKVALUE FROM JE_CORE_FUNCEDIT WHERE FUNCEDIT_USERID='"+userId+"' AND FUNCEDIT_FUNCCODE='JE_CORE_POSTIL' AND FUNCEDIT_NEW='0')");
        String querySql=" SELECT POSTIL_FUNCCODE,POSTIL_TABLECODE,POSTIL_MODELID,COUNT(*) POSTILCOUNT,MAX(SY_CREATETIME) SY_CREATETIME FROM JE_CORE_POSTIL WHERE 1=1 "+whereSql.toString()+" GROUP BY POSTIL_FUNCCODE,POSTIL_TABLECODE,POSTIL_MODELID ";
        Long count = metaService.countBySql(querySql);
        numObj.put("badge",count);
        return numObj;
    }
}
