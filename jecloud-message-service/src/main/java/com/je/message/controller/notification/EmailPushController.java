/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.message.controller.notification;

import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.google.common.base.Strings;
import com.je.common.base.constants.message.SendContextType;
import com.je.common.base.mvc.AbstractPlatformController;
import com.je.common.base.result.BaseRespResult;
import com.je.common.base.util.StringUtil;
import com.je.message.service.email.EmailService;
import com.je.message.vo.EmailMsg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "/je/message/email")
public class EmailPushController extends AbstractPlatformController {

    @Autowired
    private EmailService emailService;

    @RequestMapping(value = "/pushEmail", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult pushEmail(HttpServletRequest request) {
        //接受
        String receiveEmail = getStringParameter(request, "toReceiveEmail");
        //抄送
        String ccReceiveEmail = getStringParameter(request, "ccReceiveEmail");
        //密送
        String bccReceiveEmail = getStringParameter(request, "bccReceiveEmail");
        if (StringUtil.isEmpty(receiveEmail) && StringUtil.isEmpty(ccReceiveEmail) && StringUtil.isEmpty(bccReceiveEmail)) {
            return BaseRespResult.errorResult("发送邮箱为空！");
        }
        //主题
        String subject = getStringParameter(request, "subject");
        //内容
        String context = getStringParameter(request, "context");
        //紧急状态   1.紧急     2.普通   3.缓慢    默认2,可以为空
        String faster = getStringParameter(request, "faster");
        // 需要回执 1 回执    0 不回执     默认 0,可以为空
        String replySign = getStringParameter(request, "replySign");
        //附件内容
        String files = getStringParameter(request, "files");
        // 去掉转义字符
        files = files.replace("\\\"", "\"");
        List<String> fileNames = new ArrayList<String>();
        List<String> addresses = new ArrayList<String>();
        if (!Strings.isNullOrEmpty(files)) {
            JSONArray jsonArray = JSONArray.parseArray(files);
            for (int i = 0; i < jsonArray.size(); i++) {
                JSONObject fileJson = jsonArray.getJSONObject(i);
                addresses.add(fileJson.getString("fileKey"));
                fileNames.add(fileJson.getString("relName"));
            }
        }
        EmailMsg emailMsg = EmailMsg.build().receiveEmail(receiveEmail).ms(bccReceiveEmail).cs(ccReceiveEmail).subject(subject)
                .context(context).faster(faster).replySign(replySign).files(fileNames, addresses);
        try {
            emailService.send(emailMsg);
        } catch (Exception e) {
            e.printStackTrace();
            return BaseRespResult.errorResult(e.getMessage());
        }
        return BaseRespResult.successResult(null);
    }


    @RequestMapping(value = "/testPushEmail", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult testSystem(HttpServletRequest request) {
        String address = getStringParameter(request, "address");
        if (StringUtil.isEmpty(address)) {
            return BaseRespResult.errorResult("发送邮箱为空！");
        }
        String content = getStringParameter(request, "content");
        try {
            emailService.send(address, "测试发送服务器设置", SendContextType.HTML, content);
        } catch (Exception e) {
            e.printStackTrace();
            return BaseRespResult.errorResult(e.getMessage());
        }
        return BaseRespResult.successResult(null);
    }
}
