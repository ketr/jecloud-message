/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.message.rpc;

import com.google.common.collect.Lists;
import com.je.common.base.message.ButtonScriptBuilder;
import com.je.common.base.message.vo.*;
import com.je.common.base.service.rpc.PushMessageRpcService;
import com.je.common.base.util.SecurityUserHolder;
import org.apache.servicecomb.provider.pojo.RpcSchema;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.ArrayList;
import java.util.List;

@RpcSchema(schemaId = "socketPushMessageRpcService")
public class SocketPushMessageRpcServiceImpl implements SocketPushMessageRpcService, PushMessageRpcService {

    private static final Logger logger = LoggerFactory.getLogger(SocketPushMessageRpcServiceImpl.class);

    public static final String TOPIC = "topic.im.msg";

    @Autowired
    private RedisTemplate redisTemplate;

    @Override
    public void sendMessage(PushMessage message) {
        logger.info("send topic {} msg {} to topic {}!", TOPIC, message);
        redisTemplate.convertAndSend(TOPIC, message);
    }

    @Override
    public void sendSystemMessage(PushSystemMessage message) {
        sendMessage(message);
    }

    @Override
    public void sendScriptMessage(PushScriptMessage message) {
        sendMessage(message);
    }

    @Override
    public void sendScriptOpenFuncGridMsgToUser(String userId, String funcCode, String busType, String content) {
        PushScriptMessage pushScriptMessage = new PushScriptMessage(busType, content);
        pushScriptMessage.setTargetUserIds(Lists.newArrayList(userId));
        pushScriptMessage.setScript(ButtonScriptBuilder.buildOpenFuncGridScript(funcCode));
        pushScriptMessage.setSourceUserId(SecurityUserHolder.getCurrentAccountId());
        sendMessage(pushScriptMessage);
    }

    @Override
    public void sendScriptOpenFuncGridMsgToUsers(List<String> userIdList, String funcCode, String busType, String content) {
        PushScriptMessage pushScriptMessage = new PushScriptMessage(busType, content);
        pushScriptMessage.setTargetUserIds(Lists.newArrayList(userIdList));
        pushScriptMessage.setScript(ButtonScriptBuilder.buildOpenFuncGridScript(funcCode));
        pushScriptMessage.setSourceUserId(SecurityUserHolder.getCurrentAccountId());
        sendMessage(pushScriptMessage);
    }

    @Override
    public void sendScriptOpenFuncFormMsgToUser(String userId, String funcCode, String busType, String content, String beanId) {
        PushScriptMessage pushScriptMessage = new PushScriptMessage(busType, content);
        pushScriptMessage.setTargetUserIds(Lists.newArrayList(userId));
        pushScriptMessage.setScript(ButtonScriptBuilder.buildOpenFuncFormScript(funcCode, userId));
        pushScriptMessage.setSourceUserId(SecurityUserHolder.getCurrentAccountId());
        sendMessage(pushScriptMessage);
    }

    @Override
    public void sendScriptOpenFuncFormMsgToUsers(List<String> userIdList, String funcCode, String busType, String content, String beanId) {
        PushScriptMessage pushScriptMessage = new PushScriptMessage(busType, content);
        pushScriptMessage.setTargetUserIds(userIdList);
        pushScriptMessage.setScript(ButtonScriptBuilder.buildOpenFuncFormScript(funcCode, beanId));
        pushScriptMessage.setSourceUserId(SecurityUserHolder.getCurrentAccountId());
        sendMessage(pushScriptMessage);
    }

    @Override
    public void sendNoReadMessage(PushNoReadMessage message) {
        sendMessage(message);
    }

    @Override
    public void sendNoticeMessage(PushNoticeMessage message) {
        sendMessage(message);
    }

    @Override
    public void sendNoticeOpenFuncGridMsgToUser(String userId, String funcCode, String busType, Notice content) {
        PushNoticeMessage pushNoticeMessage = new PushNoticeMessage(busType, content);
        pushNoticeMessage.setTargetUserIds(Lists.newArrayList(userId));
        List<Notice.NoticeButton> buttons;
        if (content.getButtons() == null || !content.getButtons().isEmpty()) {
            buttons = new ArrayList<>();
            content.setButtons(buttons);
        } else {
            buttons = content.getButtons();
        }
        buttons.add(buildOpenFuncGridNoticeButton(funcCode));
        sendMessage(pushNoticeMessage);
    }

    @Override
    public void sendNoticeOpenFuncGridMsgToUsers(List<String> userIdList, String funcCode, String busType, Notice content) {
        PushNoticeMessage pushNoticeMessage = new PushNoticeMessage(busType, content);
        pushNoticeMessage.setTargetUserIds(userIdList);
        List<Notice.NoticeButton> buttons;
        if (content.getButtons() == null || !content.getButtons().isEmpty()) {
            buttons = new ArrayList<>();
            content.setButtons(buttons);
        } else {
            buttons = content.getButtons();
        }
        buttons.add(buildOpenFuncGridNoticeButton(funcCode));
        sendMessage(pushNoticeMessage);
    }

    @Override
    public void sendNoticeOpenFuncFormMsgToUser(String userId, String funcCode, String busType, Notice content, String beanId) {
        PushNoticeMessage pushNoticeMessage = new PushNoticeMessage(busType, content);
        pushNoticeMessage.setTargetUserIds(Lists.newArrayList(userId));
        List<Notice.NoticeButton> buttons;
        if (content.getButtons() == null || !content.getButtons().isEmpty()) {
            buttons = new ArrayList<>();
            content.setButtons(buttons);
        } else {
            buttons = content.getButtons();
        }
        buttons.add(buildOpenFuncFormNoticeButton(funcCode, beanId));
        sendMessage(pushNoticeMessage);
    }

    @Override
    public void sendNoticeOpenFuncFormMsgToUsers(List<String> userIdList, String funcCode, String busType, Notice content, String beanId) {
        PushNoticeMessage pushNoticeMessage = new PushNoticeMessage(busType, content);
        pushNoticeMessage.setTargetUserIds(userIdList);
        List<Notice.NoticeButton> buttons;
        if (content.getButtons() == null || !content.getButtons().isEmpty()) {
            buttons = new ArrayList<>();
            content.setButtons(buttons);
        } else {
            buttons = content.getButtons();
        }
        buttons.add(buildOpenFuncFormNoticeButton(funcCode, beanId));
        sendMessage(pushNoticeMessage);
    }

    private Notice.NoticeButton buildOpenFuncGridNoticeButton(String funcCode) {
        Notice.NoticeButton button = new Notice.NoticeButton();
        button.setText("查看");
        button.setScript(ButtonScriptBuilder.buildOpenFuncGridScript(funcCode));
        return button;
    }

    private Notice.NoticeButton buildOpenFuncFormNoticeButton(String funcCode, String beanId) {
        Notice.NoticeButton button = new Notice.NoticeButton();
        button.setText("查看");
        button.setScript(ButtonScriptBuilder.buildOpenFuncFormScript(funcCode, beanId));
        return button;
    }
}
