/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.message.rpc;

import com.je.message.vo.NoteMsg;

import java.util.Map;

/**
 * 短信服务定义
 * <p>
 * 用户自定义短信发送，需要实现此接口
 * </p>
 */
public interface NoteRpcService {

    /**
     * 发送短信
     *
     * @param msgVo 消息VO实例
     */
    boolean sendNoteMsg(NoteMsg msgVo);

    /**
     * 发送短信验证码
     *
     * @param msgVo 消息VO实例
     */
    int sendNoteCode(NoteMsg msgVo);

    /**
     * 发送短信验证码
     *
     * @param phoneNumber 手机号
     * @param fromUser    发送人
     * @param fromUserId  TODO未处理
     * @param toUser      接收人
     * @param toUserId    TODO未处理
     * @param context     内容
     */
    int sendWithRecordSendCode(String phoneNumber, String fromUser, String fromUserId, String toUser, String toUserId, String context);

    /**
     * 发送短信
     *
     * @param phoneNumber 手机号
     * @param fromUser    发送人
     * @param fromUserId  TODO未处理
     * @param toUser      接收人
     * @param toUserId    TODO未处理
     * @param context     内容
     */
    boolean sendWithRecordSendUser(String phoneNumber, String fromUser, String fromUserId, String toUser, String toUserId, String context);

    /**
     * 发送短信使用模版
     *
     * @param phoneNumber  手机号
     * @param fromUser     发送人名称
     * @param fromUserId   发送人id
     * @param toUser       接收人名称
     * @param toUserId     接收人id
     * @param context      内容，使用模版可以为空
     * @param serviceType  服务商类型
     * @param signName     签名
     * @param templateCode 模版编码
     * @param params       参数
     * @return
     */
    boolean sendWithRecordSendUserByTemplateCode(String phoneNumber, String fromUser, String fromUserId, String toUser, String toUserId, String context
            , String serviceType, String signName, String templateCode, Map<String, String> params);

    /**
     * 发送短信
     *
     * @param phoneNumber 手机号
     * @param context     内容
     */
    boolean sendSimple(String phoneNumber, String context);

    /**
     * 数据模版
     *
     * @param phoneNumber 电话
     * @param tempCode    TODO未处理
     * @param params      传入信息
     */
    boolean sendTemplate(String phoneNumber, String tempCode, Map<String, Object> params);

    /**
     * 发送短信
     *
     * @param phoneNumber
     * @param context
     * @return
     */
    int sendNote(String phoneNumber, String context);

    /**
     * 发送短信
     */
    boolean sendNoteByTemplateCode(String number, String context, String signName, String templateCode, Map<String, String> params);

}
