package com.je.message.vo.weWork;

public class WeWorkTextNotice extends AbstractWeWorkNotice {
    /**
     * 消息内容
     */
    private String content;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
