package com.je.message.controller;

import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.je.common.base.result.BaseRespResult;
import com.je.common.base.util.SecurityUserHolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import javax.servlet.http.HttpServletRequest;
import static com.je.message.logback.RealtimeLogAppender.*;

@RestController
@RequestMapping("/je/message/log/realtime")
public class RealtimeLogController {

    private static final Logger logger = LoggerFactory.getLogger(RealtimeLogController.class);

    @RequestMapping(value = "/status", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult status(HttpServletRequest request) {
        String userId = request.getParameter("userId");
        if (Strings.isNullOrEmpty(userId)) {
            userId = SecurityUserHolder.getCurrentAccountRealUserId();
        }
        if (LOG_APPEND_USERS.contains(userId)) {
            return BaseRespResult.successResult("ok");
        } else {
            return BaseRespResult.errorResult("no");
        }
    }

    @RequestMapping(value = "/regist", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult regist(HttpServletRequest request) {
        String userId = request.getParameter("userId");
        String onlySql = request.getParameter("onlySql");
        String formatSql = request.getParameter("formatSql");
        String forAllUser = request.getParameter("forAllUser");
        String speciedUserIds = request.getParameter("speciedUserIds");

        if (Strings.isNullOrEmpty(userId)) {
            userId = SecurityUserHolder.getCurrentAccountRealUserId();
        }

        logger.info("regist userId:{},onlySql:{},formatSql:{},forAllUser", userId, onlySql, formatSql, forAllUser);

        LOG_CONTROL_VO.setOnlySql("1".equals(onlySql));
        LOG_CONTROL_VO.setFormatSql("1".equals(formatSql));
        LOG_CONTROL_VO.setForAllUser("1".equals(forAllUser));
        if (!Strings.isNullOrEmpty(speciedUserIds)) {
            LOG_CONTROL_VO.setSpectialUserList(Splitter.on(",").splitToList(speciedUserIds));
        }
        if (!LOG_APPEND_USERS.contains(userId)) {
            LOG_APPEND_USERS.add(userId);
        }
        return BaseRespResult.successResult("ok");
    }

    @RequestMapping(value = "/unregist", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult unregist(HttpServletRequest request) {
        String userId = request.getParameter("userId");
        if (Strings.isNullOrEmpty(userId)) {
            userId = SecurityUserHolder.getCurrentAccountRealUserId();
        }
        LOG_APPEND_USERS.remove(userId);
        return BaseRespResult.successResult("ok");
    }

}
