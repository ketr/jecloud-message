/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.message.model;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import com.je.common.base.DynaBean;
import java.io.Serializable;

/**
 * 模板实体
 *
 * @author wangmm@ketr.com.cn
 * @date 2020/5/7
 */
public class Template implements Serializable {

    private static final long serialVersionUID = -1L;

    /**
     * 主键
     */
    private String id;
    /**
     * 业务类别
     */
    private String category;
    /**
     * 平台类型
     */
    private String platform;
    /**
     * 标题
     */
    private String title;
    /**
     * 内容
     */
    private String content;
    /**
     * 按钮文本
     */
    private String buttonText;
    /**
     * 按钮事件或url
     */
    private String buttonAction;
    /**
     * 拓展参数
     */
    private JSONObject params = new JSONObject();

    public static Template transform(DynaBean bean) {
        Template template = new Template();
        template.setId(bean.getStr("JE_CORE_PUSH_TEMPLATE_ID"));
        template.setCategory(bean.getStr("TEMPLATE_CATEGORY"));
        template.setPlatform(bean.getStr("TEMPLATE_PLATFORM"));
        template.setTitle(bean.getStr("TEMPLATE_TITLE"));
        template.setContent(bean.getStr("TEMPLATE_CONTENT"));
        template.setButtonText(bean.getStr("TEMPLATE_BUTTON_TEXT"));
        template.setButtonAction(bean.getStr("TEMPLATE_BUTTON_ACTION"));
        try {
            template.setParams(JSON.parseObject(bean.getStr("TEMPLATE_PARAMS")));
        } catch (Exception ignored) {
        }
        return template;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getButtonText() {
        return buttonText;
    }

    public void setButtonText(String buttonText) {
        this.buttonText = buttonText;
    }

    public String getButtonAction() {
        return buttonAction;
    }

    public void setButtonAction(String buttonAction) {
        this.buttonAction = buttonAction;
    }

    public JSONObject getParams() {
        return params;
    }

    public void setParams(JSONObject params) {
        this.params = params;
    }
}