/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.message.vo;

import com.je.common.base.constants.message.SendContextType;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @ClassName
 * @Author wangchao
 * @Date 2022/4/27 0027 10:32
 * @Version V1.0
 */
public class LogMsg {
    /**
     * 发送人
     */
    private String fromUser;
    /**
     * 发送人ID
     */
    private String fromUserId;
    /**
     * 接受人
     */
    private String toUser;
    /**
     * 接受人ID
     */
    private String toUserId;
    /**
     * 接收部门ID
     */
    private String deptIds;
    /**
     * 发送内容
     */
    private String context;
    /**
     * 分类内容
     */
    private String fl;
    /**
     * 手机号
     */
    private String phoneNumber;
    /**
     * 发送结果
     */
    private String result;
    /**
     * 错误信息
     */
    private String errorMessage;
    /**
     * 接受邮箱
     */
    private String receiveEmail;
    /**
     * 主题
     */
    private String subject;
    /**
     * 内容类型
     */
    private String contextType;
    /**
     * 文件名称
     */
    private List<String> fileNames = new ArrayList<String>();
    /**
     * 文件地址
     */
    private List<String> addresses = new ArrayList<String>();
    /**
     * 文件地址
     */
    private String fileKey;
    /**
     * 消息标题
     */
    private String title;
    /**
     * 链接url
     */
    private String url;
    /**
     * 原文链接地址
     */
    private String sourceUrl;
    /**
     * 原文链接地址
     */
    private String buttonText;

    public String getFromUser() {
        return fromUser;
    }

    public void setFromUser(String fromUser) {
        this.fromUser = fromUser;
    }

    public String getFromUserId() {
        return fromUserId;
    }

    public void setFromUserId(String fromUserId) {
        this.fromUserId = fromUserId;
    }

    public String getToUser() {
        return toUser;
    }

    public void setToUser(String toUser) {
        this.toUser = toUser;
    }

    public String getToUserId() {
        return toUserId;
    }

    public void setToUserId(String toUserId) {
        this.toUserId = toUserId;
    }

    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }

    public String getFl() {
        return fl;
    }

    public void setFl(String fl) {
        this.fl = fl;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getReceiveEmail() {
        return receiveEmail;
    }

    public void setReceiveEmail(String receiveEmail) {
        this.receiveEmail = receiveEmail;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getContextType() {
        return contextType;
    }

    public void setContextType(String contextType) {
        this.contextType = contextType;
    }

    public String getDeptIds() {
        return deptIds;
    }

    public void setDeptIds(String deptIds) {
        this.deptIds = deptIds;
    }

    public List<String> getFileNames() {
        return fileNames;
    }

    public void setFileNames(List<String> fileNames) {
        this.fileNames = fileNames;
    }

    public List<String> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<String> addresses) {
        this.addresses = addresses;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getSourceUrl() {
        return sourceUrl;
    }

    public void setSourceUrl(String sourceUrl) {
        this.sourceUrl = sourceUrl;
    }

    public String getFileKey() {
        return fileKey;
    }

    public void setFileKey(String fileKey) {
        this.fileKey = fileKey;
    }

    public String getButtonText() {
        return buttonText;
    }

    public void setButtonText(String buttonText) {
        this.buttonText = buttonText;
    }

    @Override
    public String toString() {
        return "LogMsg{" +
                "fromUser='" + fromUser + '\'' +
                ", fromUserId='" + fromUserId + '\'' +
                ", toUser='" + toUser + '\'' +
                ", toUserId='" + toUserId + '\'' +
                ", deptIds='" + deptIds + '\'' +
                ", context='" + context + '\'' +
                ", fl='" + fl + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", result='" + result + '\'' +
                ", errorMessage='" + errorMessage + '\'' +
                ", receiveEmail='" + receiveEmail + '\'' +
                ", subject='" + subject + '\'' +
                ", contextType='" + contextType + '\'' +
                ", fileNames=" + fileNames +
                ", addresses=" + addresses +
                ", fileKey='" + fileKey + '\'' +
                ", title='" + title + '\'' +
                ", url='" + url + '\'' +
                ", sourceUrl='" + sourceUrl + '\'' +
                ", buttonText='" + buttonText + '\'' +
                '}';
    }
}
