package com.je.message.vo.dingTalk;

/**
 * 文本消息
 */
public class DingTalkTextWorkNotice extends AbstractDingTalkWorkNotice {

    private String content;

    private DingTalkMsgTypeEnum type;

    private DingTalkTextWorkNotice() {
        super(DingTalkMsgTypeEnum.TEXT, "");
    }

    public DingTalkTextWorkNotice(String pkValue) {
        super(DingTalkMsgTypeEnum.TEXT, pkValue);
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }


}
