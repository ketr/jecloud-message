package com.je.message.vo.dingTalk;

/**
 * 文本消息
 */
public class DingTalkMarkDownWorkNotice extends AbstractDingTalkWorkNotice {

    private String title;

    private String content;

    private DingTalkMarkDownWorkNotice() {
        super(DingTalkMsgTypeEnum.MARKDOWN, "");
    }

    public DingTalkMarkDownWorkNotice(String pkValue) {
        super(DingTalkMsgTypeEnum.MARKDOWN, pkValue);
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}
