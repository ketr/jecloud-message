/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.message.controller.notification;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUtil;
import com.alibaba.fastjson2.JSONObject;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.je.common.base.DynaBean;
import com.je.common.base.mapper.query.NativeQuery;
import com.je.common.base.message.vo.Notice;
import com.je.common.base.message.vo.NoticeMsg;
import com.je.common.base.message.vo.PushSystemMessage;
import com.je.common.base.mvc.AbstractPlatformController;
import com.je.common.base.mvc.BaseMethodArgument;
import com.je.common.base.result.BaseRespResult;
import com.je.common.base.service.CommonService;
import com.je.common.base.service.MetaRbacService;
import com.je.common.base.service.MetaService;
import com.je.common.base.util.SecurityUserHolder;
import com.je.common.base.util.StringUtil;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.ibatis.extension.plugins.pagination.Page;
import com.je.message.rpc.SocketPushMessageRpcService;
import com.je.message.service.portal.PortalService;
import com.je.message.vo.WebPushTypeEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @ClassName
 * @Author wangchao
 * @Date 2022/5/31 0031 10:05
 * @Version V1.0
 */
@RestController
@RequestMapping(value = "/je/message/myShare")
public class MyShareController extends AbstractPlatformController {
    private Logger logger = LoggerFactory.getLogger(HomeNotificationController.class);

    @Autowired
    private MetaService metaService;

    @Autowired
    private MetaRbacService metaRbacService;

    @Autowired
    private CommonService commonService;

    @Autowired
    private SocketPushMessageRpcService socketPushMessageRpcService;

    @Autowired
    private PortalService portalService;

    /**
     * @param
     * @note 保存我的分享
     * @date
     */
    @RequestMapping(value = "/saveShare", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public BaseRespResult saveShare(BaseMethodArgument param, HttpServletRequest request) {
        String title = getStringParameter(request, "SHARE_TITLE");
        String funcName = getStringParameter(request, "funcName");
        String funcCode = getStringParameter(request, "funcCode");
        String pkValue = getStringParameter(request, "pkValue");
        String tableCode = getStringParameter(request, "tableCode");
        String accountIds = getStringParameter(request, "accountIds");
        String userNames = getStringParameter(request, "userNames");
        DynaBean dynaBean = new DynaBean("JE_MESSAGE_SHARE", false);
        dynaBean.set("SHARE_TITLE", title);
        dynaBean.set("SHARE_YWSJZJ", pkValue);
        dynaBean.set("SHARE_TABLE_CODE", tableCode);
        dynaBean.set("SHARE_FUNC_NAME", funcName);
        dynaBean.set("SHARE_FUNC_CODE", funcCode);
        commonService.buildModelCreateInfo(dynaBean);
        metaService.insert(dynaBean);
        //我的分享列表监听刷新
        Notice noticeMyShare = new Notice();
        noticeMyShare.setPlayAudio(true);
        JSONObject jsonObjectMyShare = new JSONObject();
        jsonObjectMyShare.put("code", "COLLECTION");
        jsonObjectMyShare.put("type", "MSG_MYSHARE");
        jsonObjectMyShare.put("data", dynaBean);
        jsonObjectMyShare.put("notify", noticeMyShare);
        PushSystemMessage pushSystemMessageMyShare = new PushSystemMessage("", String.valueOf(jsonObjectMyShare));
        pushSystemMessageMyShare.setTargetUserIds(Lists.newArrayList(SecurityUserHolder.getCurrentAccountRealUserId()));
        pushSystemMessageMyShare.setSourceUserId("系统");
        socketPushMessageRpcService.sendMessage(pushSystemMessageMyShare);
        //被分享人表
        String zhuId = dynaBean.getStr("JE_MESSAGE_SHARE_ID");
        DynaBean userBean = null;
        String[] userIdArray = accountIds.split(",");
        String[] userNameArray = userNames.split(",");
        DynaBean accountDeptDynaBean = metaRbacService.selectOne("JE_RBAC_VACCOUNTDEPT", NativeQuery.build().eq("USER_ASSOCIATION_ID", SecurityUserHolder.getCurrentAccountRealUserId()).eq("ACCOUNTDEPT_DEPT_ID", SecurityUserHolder.getCurrentAccountDepartment().getId()), "ACCOUNT_AVATAR");
        for (int i = 0; i < userIdArray.length; i++) {
            userBean = new DynaBean("JE_MESSAGE_SHAREDUSER", false);
            userBean.set("JE_MESSAGE_SHARE_ID", zhuId);
            userBean.set("SHAREDUSER_USER_ID", userIdArray[i]);
            userBean.set("SHAREDUSER_USER_NAME", userNameArray[i]);
            userBean.set("SHAREDUSER_YDZT_CODE", "0");
            userBean.set("SHAREDUSER_AVATAR", accountDeptDynaBean == null ? "" : accountDeptDynaBean.getStr("ACCOUNT_AVATAR"));
            commonService.buildModelCreateInfo(userBean);
            metaService.insert(userBean);
            //获取正式账户id，发送通知 TODO 提醒内容 修改
            Notice notice = new Notice();
            notice.setTitle("共享业务数据提醒");
            notice.setContent("" + DateUtil.now() + "，" + SecurityUserHolder.getCurrentAccountRealUserName() + "向您共享业务数据【" + title + "】，请关注！");
            notice.setPlayAudio(true);
            notice.setNowTime(DateUtil.now());
            DynaBean deptUserBean = metaRbacService.selectOne("JE_RBAC_VACCOUNTDEPT", NativeQuery.build().eq("JE_RBAC_ACCOUNTDEPT_ID", userIdArray[i]), "JE_RBAC_ACCOUNT_ID,ACCOUNTDEPT_DEPT_ID");
            if (deptUserBean == null) {
                continue;
            }
            //右上角通知 添加未读
            portalService.insertOrUpdateSign(deptUserBean.getStr("USER_ASSOCIATION_ID"), deptUserBean.getStr("ACCOUNTDEPT_DEPT_ID"), WebPushTypeEnum.MSG, "insert");
            //右上角通知 监听
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("code", "SHARED");
            jsonObject.put("type", "MSG_SHARED");
            dynaBean.setStr("SHAREDUSER_AVATAR", accountDeptDynaBean == null ? "" : accountDeptDynaBean.getStr("ACCOUNT_AVATAR"));
            jsonObject.put("data", dynaBean);
            jsonObject.put("notify", notice);
            PushSystemMessage pushSystemMessage = new PushSystemMessage("MSG", String.valueOf(jsonObject));
            pushSystemMessage.setTargetUserIds(Lists.newArrayList(deptUserBean.getStr("USER_ASSOCIATION_ID")));
            pushSystemMessage.setSourceUserId(SecurityUserHolder.getCurrentAccountId());
            socketPushMessageRpcService.sendMessage(pushSystemMessage);
            //通知列表添加
            NoticeMsg noticeMsg = new NoticeMsg(deptUserBean.getStr("USER_ASSOCIATION_ID"), deptUserBean.getStr("ACCOUNT_NAME"), deptUserBean.getStr("ACCOUNTDEPT_DEPT_ID"), deptUserBean.getStr("ACCOUNTDEPT_DEPT_NAME"),
                    "" + DateUtil.now() + "，" + SecurityUserHolder.getCurrentAccountRealUserName() + "向您共享业务数据【" + title + "】，请关注！", "共享业务数据提醒", "SHARED", "共享");
            portalService.insertNoticeMsg(noticeMsg);
            //右下角通知
            socketPushMessageRpcService.sendNoticeOpenFuncFormMsgToUser(deptUserBean.getStr("USER_ASSOCIATION_ID"), funcCode, "", notice, pkValue);
        }
        return BaseRespResult.successResult("保存成功！");
    }

    private final String time = "time";
    private final String func = "func";

    /**
     * @param
     * @note 加载首页我的共享
     * @date
     */
    @RequestMapping(value = "/loadMyShare", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public BaseRespResult loadMyShare(BaseMethodArgument param, HttpServletRequest request) {
        String userId = getStringParameter(request, "userId");
        String deptId = getStringParameter(request, "deptId");
        if (Strings.isNullOrEmpty(userId) || Strings.isNullOrEmpty(deptId)) {
            userId = SecurityUserHolder.getCurrentAccountRealUserId();
            deptId = SecurityUserHolder.getCurrentAccountRealOrgId();
        }
        String keyWord = getStringParameter(request, "keyWord");
        String oderType = getStringParameter(request, "oderType");
        ConditionsWrapper whereSqlWrapper = ConditionsWrapper.builder().table("JE_MESSAGE_SHARE");
        whereSqlWrapper.apply("AND SY_CREATEUSERID = {0} AND SY_CREATEORGID = {1}", userId, deptId);
        if (StringUtil.isNotEmpty(keyWord)) {
            whereSqlWrapper.apply("AND ( SHARE_TITLE like {0} OR SHARE_FUNC_NAME like {1} )", "%" + keyWord + "%", "%" + keyWord + "%");
        }
        if (StringUtil.isNotEmpty(oderType) && time.equals(oderType)) {
            whereSqlWrapper.apply("order by SY_CREATETIME desc");
        }
        if (StringUtil.isNotEmpty(oderType) && func.equals(oderType)) {
            whereSqlWrapper.apply("order by SHARE_FUNC_CODE desc");
        }
        int pageNum = param.getPage();
        int limit = param.getLimit();
        long count = 0L;
        int currentPage = 0;
        int pages = 0;
        List<DynaBean> list;
        if (limit == -1) {
            list = metaService.select(whereSqlWrapper);
            count = list.size();

        } else {
            Page page = new Page(pageNum, limit);
            list = metaService.select("JE_MESSAGE_SHARE", page, whereSqlWrapper);
            count = page.getTotal();
            currentPage = page.getCurrent();
            pages = page.getPages();
        }
        JSONObject returnObj = new JSONObject();
        List<HashMap> values = new ArrayList<>();
        for (DynaBean bean : list) {
            values.add(bean.getValues());
        }
        returnObj.put("rows", values);
        returnObj.put("totalCount", count);
        returnObj.put("currentPage", currentPage);
        returnObj.put("pages", pages);
        return BaseRespResult.successResult(returnObj);
    }

    /**
     * @param
     * @note 加载首页共享给我
     * @date
     */
    @RequestMapping(value = "/loadShareMe", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public BaseRespResult loadShareMe(BaseMethodArgument param, HttpServletRequest request) {
        String userId = getStringParameter(request, "userId");
        String deptId = getStringParameter(request, "deptId");
        if (Strings.isNullOrEmpty(userId) || Strings.isNullOrEmpty(deptId)) {
            userId = SecurityUserHolder.getCurrentAccountRealUserId();
            deptId = SecurityUserHolder.getCurrentAccountRealOrgId();
        }
        String keyWord = getStringParameter(request, "keyWord");
        String oderType = getStringParameter(request, "oderType");

        ConditionsWrapper wrapper = ConditionsWrapper.builder().table("JE_MESSAGE_SHARE");

        //构建返回
        JSONObject returnObj = new JSONObject();
        //获取账号部门视图ID主键
        DynaBean deptUserBean = metaRbacService.selectOne("JE_RBAC_VACCOUNTDEPT", NativeQuery.build().eq("USER_ASSOCIATION_ID", userId).eq("ACCOUNTDEPT_DEPT_ID", deptId), "JE_RBAC_ACCOUNTDEPT_ID");
        if (deptUserBean == null) {
            return BaseRespResult.errorResult("人员信息出错！请联系管理员！");
        }
        String accountDeptId = deptUserBean.getStr("JE_RBAC_ACCOUNTDEPT_ID");
        if (StringUtil.isEmpty(accountDeptId)) {
            return BaseRespResult.errorResult("人员信息出错！请联系管理员！");
        }
        List<DynaBean> dynaBeanList = metaService.select("JE_MESSAGE_SHAREDUSER", ConditionsWrapper.builder().eq("SHAREDUSER_USER_ID", accountDeptId));
        if (dynaBeanList == null || dynaBeanList.size() == 0) {
            returnObj.put("rows", new ArrayList<>());
            return BaseRespResult.successResult(returnObj);
        }

        List<String> shareLists = new ArrayList<>();
        int notRead = 0;
        for (DynaBean dynaBean : dynaBeanList) {
            shareLists.add(dynaBean.getStr("JE_MESSAGE_SHARE_ID"));
            String readType = dynaBean.getStr("SHAREDUSER_YDZT_CODE");
            if ("0".equals(readType)) {
                notRead++;
            }
        }
        String shareIds = String.join(",", shareLists);
        wrapper.apply("AND JE_MESSAGE_SHARE_ID IN (" + StringUtil.buildArrayToString(shareIds.split(",")) + ")");
        if (StringUtil.isNotEmpty(keyWord)) {
            wrapper.apply("AND ( SHARE_TITLE like {0} OR SHARE_FUNC_NAME like {1} )", "%" + keyWord + "%", "%" + keyWord + "%");
        }
        if (StringUtil.isNotEmpty(oderType) && time.equals(oderType)) {
            wrapper.apply("order by SY_CREATETIME desc");
        }
        if (StringUtil.isNotEmpty(oderType) && func.equals(oderType)) {
            wrapper.apply("order by SHARE_FUNC_CODE desc");
        }

        int pageNum = param.getPage();
        int limit = param.getLimit();
        long count = 0L;
        int currentPage = 0;
        int pages = 0;
        List<DynaBean> list;
        if (limit == -1) {
            list = metaService.select(wrapper);
            count = list.size();

        } else {
            Page page = new Page(pageNum, limit);
            list = metaService.select("JE_MESSAGE_SHARE", page, wrapper);
            count = page.getTotal();
            currentPage = page.getCurrent();
            pages = page.getPages();
        }

        if (list == null || list.size() == 0) {
            returnObj.put("rows", new ArrayList());
            return BaseRespResult.successResult(returnObj);
        }
        List<HashMap> values = new ArrayList<>();
        for (DynaBean bean : list) {

            DynaBean dynaBean = metaService.selectOne("JE_MESSAGE_SHAREDUSER", ConditionsWrapper.builder().eq("JE_MESSAGE_SHARE_ID", bean.getStr("JE_MESSAGE_SHARE_ID")).eq("SHAREDUSER_USER_ID", accountDeptId));
            bean.setStr("SHAREDUSER_YDZT_CODE", dynaBean.getStr("SHAREDUSER_YDZT_CODE"));
            DynaBean accountDeptDynaBean = metaRbacService.selectOne("JE_RBAC_VACCOUNTDEPT", NativeQuery.build().eq("USER_ASSOCIATION_ID", dynaBean.getStr("SY_CREATEUSERID")).eq("ACCOUNTDEPT_DEPT_ID", dynaBean.getStr("SY_CREATEORGID")), "ACCOUNT_AVATAR");
            bean.setStr("SHAREDUSER_AVATAR", accountDeptDynaBean == null ? "" : accountDeptDynaBean.getStr("ACCOUNT_AVATAR"));
            values.add(bean.getValues());
        }
        returnObj.put("rows", values);
        returnObj.put("totalCount", count);
        returnObj.put("currentPage", currentPage);
        returnObj.put("pages", pages);
        returnObj.put("notRead", notRead);
        return BaseRespResult.successResult(returnObj);
    }

    /**
     * @param
     * @note 删除或者全部删除 我的分享
     * @date
     */
    @RequestMapping(value = "/deleteMyShare", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public BaseRespResult deleteMyShare(BaseMethodArgument param, HttpServletRequest request) {
        String pkValue = getStringParameter(request, "JE_MESSAGE_SHARE_ID");
        String userId = getStringParameter(request, "userId");
        String deptId = getStringParameter(request, "deptId");
        if (Strings.isNullOrEmpty(userId) || Strings.isNullOrEmpty(deptId)) {
            userId = SecurityUserHolder.getCurrentAccountRealUserId();
            deptId = SecurityUserHolder.getCurrentAccountRealOrgId();
        }
        if (StringUtil.isNotEmpty(pkValue)) {
            int num = metaService.delete("JE_MESSAGE_SHARE", ConditionsWrapper.builder().eq("JE_MESSAGE_SHARE_ID", pkValue).eq("SY_CREATEUSERID", userId));
            int i = metaService.delete("JE_MESSAGE_SHAREDUSER", ConditionsWrapper.builder().eq("JE_MESSAGE_SHARE_ID", pkValue));
            return BaseRespResult.successResult(null);
        }
        List<DynaBean> dynaBeanList = metaService.select("JE_MESSAGE_SHARE", ConditionsWrapper.builder().eq("SY_CREATEUSERID", userId).eq("SY_CREATEORGID", deptId));
        List<String> shareIdList = CollUtil.newArrayList();

        if (dynaBeanList != null && dynaBeanList.size() > 0) {
            for (DynaBean dynaBean : dynaBeanList) {
                shareIdList.add(dynaBean.getStr("JE_MESSAGE_SHARE_ID"));
            }
        }
        String shareIds = String.join(",", shareIdList);
        int i = metaService.delete("JE_MESSAGE_SHAREDUSER", ConditionsWrapper.builder().apply("JE_MESSAGE_SHARE_ID IN (" + StringUtil.buildArrayToString(shareIds.split(",")) + ")"));
        int num = metaService.delete("JE_MESSAGE_SHARE", ConditionsWrapper.builder().eq("SY_CREATEUSERID", userId).eq("SY_CREATEORGID", deptId));

        return BaseRespResult.successResult(null);
    }


    /**
     * @param
     * @note 删除或者全部删除 分享给我的
     * @date
     */
    @RequestMapping(value = "/deleteShareMe", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public BaseRespResult deleteShareMe(BaseMethodArgument param, HttpServletRequest request) {
        String pkValue = getStringParameter(request, "JE_MESSAGE_SHARE_ID");
        String userId = getStringParameter(request, "userId");
        String deptId = getStringParameter(request, "deptId");
        if (Strings.isNullOrEmpty(userId) || Strings.isNullOrEmpty(deptId)) {
            userId = SecurityUserHolder.getCurrentAccountRealUserId();
            deptId = SecurityUserHolder.getCurrentAccountRealOrgId();
        }
        //获取账号部门视图ID主键
        DynaBean deptUserBean = metaRbacService.selectOne("JE_RBAC_VACCOUNTDEPT", NativeQuery.build().eq("USER_ASSOCIATION_ID", userId).eq("ACCOUNTDEPT_DEPT_ID", deptId), "JE_RBAC_ACCOUNTDEPT_ID");
        if (deptUserBean == null) {
            return BaseRespResult.errorResult("人员信息出错！请联系管理员！");
        }
        String accountDeptId = deptUserBean.getStr("JE_RBAC_ACCOUNTDEPT_ID");
        if (StringUtil.isEmpty(accountDeptId)) {
            return BaseRespResult.errorResult("人员信息出错！请联系管理员！");
        }

        if (StringUtil.isNotEmpty(pkValue)) {
            int i = metaService.delete("JE_MESSAGE_SHAREDUSER", ConditionsWrapper.builder().eq("JE_MESSAGE_SHARE_ID", pkValue).eq("SHAREDUSER_USER_ID", accountDeptId));
            return BaseRespResult.successResult(null);
        }

        int i = metaService.delete("JE_MESSAGE_SHAREDUSER", ConditionsWrapper.builder().eq("SHAREDUSER_USER_ID", accountDeptId));

        return BaseRespResult.successResult(null);
    }

    /**
     * @param
     * @note 修改分享给我的阅读状态
     * @date
     */
    @RequestMapping(value = "/updateShareMeRead", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public BaseRespResult updateShareMeRead(BaseMethodArgument param, HttpServletRequest request) {
        String pkValue = getStringParameter(request, "JE_MESSAGE_SHARE_ID");
        String userId = getStringParameter(request, "userId");
        String deptId = getStringParameter(request, "deptId");
        if (Strings.isNullOrEmpty(userId) || Strings.isNullOrEmpty(deptId)) {
            userId = SecurityUserHolder.getCurrentAccountRealUserId();
            deptId = SecurityUserHolder.getCurrentAccountRealOrgId();
        }
        //获取账号部门视图ID主键
        DynaBean deptUserBean = metaRbacService.selectOne("JE_RBAC_VACCOUNTDEPT", NativeQuery.build().eq("USER_ASSOCIATION_ID", userId).eq("ACCOUNTDEPT_DEPT_ID", deptId), "JE_RBAC_ACCOUNTDEPT_ID");
        if (deptUserBean == null) {
            return BaseRespResult.errorResult("人员信息出错！请联系管理员！");
        }
        String accountDeptId = deptUserBean.getStr("JE_RBAC_ACCOUNTDEPT_ID");
        if (StringUtil.isEmpty(accountDeptId)) {
            return BaseRespResult.errorResult("人员信息出错！请联系管理员！");
        }
        List<DynaBean> dynaBeanList = null;
        if (StringUtil.isEmpty(pkValue)) {
            dynaBeanList = metaService.select("JE_MESSAGE_SHAREDUSER", ConditionsWrapper.builder().eq("SHAREDUSER_USER_ID", accountDeptId));
        } else {
            dynaBeanList = metaService.select("JE_MESSAGE_SHAREDUSER", ConditionsWrapper.builder().eq("SHAREDUSER_USER_ID", accountDeptId).eq("JE_MESSAGE_SHARE_ID", pkValue));
        }
        if (dynaBeanList != null && dynaBeanList.size() > 0) {
            for (DynaBean dynaBean : dynaBeanList) {
                dynaBean.set("SHAREDUSER_YDZT_CODE", "1");
                metaService.update(dynaBean);
            }
        }
        return BaseRespResult.successResult("修改成功！");
    }




    /*--------------------------------------------------------------------------------------------------------*/


    /**
     * @param
     * @note 加载我的分享 作废不要回显 先保留以防以后要
     * @date
     */
    @RequestMapping(value = "/loadShare", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public BaseRespResult loadShare(BaseMethodArgument param, HttpServletRequest request) {
        //我的分享表主键
        String pkValue = getStringParameter(request, "JE_MESSAGE_SHARE_ID");
        DynaBean dynaBean = metaService.selectOne("JE_MESSAGE_SHARE", ConditionsWrapper.builder().eq("JE_MESSAGE_SHARE_ID", pkValue));
        if (dynaBean == null) {
            return BaseRespResult.successResult(null);
        }
        //被分享人信息
        List<DynaBean> dynaBeanList = metaService.select("JE_MESSAGE_SHAREDUSER", ConditionsWrapper.builder().eq("JE_MESSAGE_SHARE_ID", pkValue));

        JSONObject returnObj = new JSONObject();
        returnObj.put("title", dynaBean.getStr("SHARE_TITLE"));
        if (dynaBeanList != null && dynaBeanList.size() > 0) {
            for (DynaBean bean : dynaBeanList) {
                returnObj.put("userId", bean.getStr("SHAREDUSER_USER_ID"));
                returnObj.put("UserName", bean.getStr("SHAREDUSER_USER_NAME"));
            }
        }

        return BaseRespResult.successResult(returnObj);
    }


    /**
     * @param
     * @note 修改作废！！！先保留以防以后要
     * @date
     */
    @RequestMapping(value = "/doUpdateShare", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public BaseRespResult doUpdateShare(BaseMethodArgument param, HttpServletRequest request) {
        String title = getStringParameter(request, "SHARE_TITLE");
        String funcName = getStringParameter(request, "funcName");
        String funcCode = getStringParameter(request, "funcCode");
        String pkValue = getStringParameter(request, "pkValue");
        String tableCode = getStringParameter(request, "tableCode");
        String userIds = getStringParameter(request, "userIds");
        String shareId = getStringParameter(request, "JE_MESSAGE_SHARE_ID");
        if (StringUtil.isEmpty(shareId)) {
            return BaseRespResult.errorResult("数据主键为空！");
        }
        DynaBean dynaBean = metaService.selectOne("JE_MESSAGE_SHARE", ConditionsWrapper.builder().eq("JE_MESSAGE_SHARE_ID", shareId));
        if (dynaBean == null) {
            logger.info("数据出错，没有查到这条数据！表名是<JE_MESSAGE_SHARE>,主键是" + shareId + "");
            return BaseRespResult.errorResult("数据出现错误！");
        }
        dynaBean.set("SHARE_TITLE", title);
        dynaBean.set("SHARE_YWSJZJ", pkValue);
        dynaBean.set("SHARE_TABLE_CODE", tableCode);
        dynaBean.set("SHARE_FUNC_NAME", funcName);
        dynaBean.set("SHARE_FUNC_CODE", funcCode);
        metaService.update(dynaBean);
        //被分享人表修改
        String zhuId = dynaBean.getPkValue();
        DynaBean userBean = null;
        String[] userIdArray = userIds.split(",");
        List<DynaBean> allUserBeanList = metaService.select("JE_MESSAGE_SHAREDUSER", ConditionsWrapper.builder().eq("JE_MESSAGE_SHARE_ID", zhuId));
        ArrayList<String> idList = new ArrayList<>();
        for (DynaBean bean : allUserBeanList) {
            for (String id : userIdArray) {
                if (!id.equals(bean.getStr("JE_MESSAGE_SHAREDUSER"))) {
                    idList.add(bean.getStr("JE_MESSAGE_SHAREDUSER"));
                }
            }
        }
        metaService.delete("JE_MESSAGE_SHAREDUSER", ConditionsWrapper.builder().in("JE_MESSAGE_SHAREDUSER", idList));

        boolean isEmpty = false;
        for (String id : userIdArray) {
            userBean = metaService.selectOne("JE_MESSAGE_SHAREDUSER", ConditionsWrapper.builder().eq("JE_MESSAGE_SHARE_ID", zhuId).eq("SHAREDUSER_USER_ID", id));
            if (userBean == null) {
                userBean = new DynaBean("JE_MESSAGE_SHAREDUSER", false);
                isEmpty = true;
            }
            userBean.set("JE_MESSAGE_SHARE_ID", zhuId);
            userBean.set("SHAREDUSER_USER_ID", id);
            userBean.set("SHAREDUSER_YDZT_CODE", "0");
            if (!isEmpty) {
                continue;
            }
            commonService.buildModelCreateInfo(userBean);
            metaService.insert(userBean);
            //获取正式账户id，发送通知
            DynaBean deptUserBean = metaService.selectOne("JE_RBAC_VACCOUNTDEPT", ConditionsWrapper.builder().eq("JE_RBAC_ACCOUNTDEPT_ID", id));
//            pushRpcService.sendMsg(deptUserBean.getStr("JE_RBAC_ACCOUNT_ID"), deptUserBean.getStr("ACCOUNTDEPT_DEPT_ID"), "消息提醒", "您的朋友为您分享一条信息，请在首页查看！");
        }
        return BaseRespResult.successResult("保存成功！");
    }
}
