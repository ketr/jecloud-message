/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.message.service.log;

import com.je.common.base.spring.SpringContextHolder;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @ClassName
 * @Author wangchao
 * @Date 2022/4/27 0027 9:34
 * @Version V1.0
 */
public class LogServiceFactory {

    private static final Map<LogTypeEnum, AbstractLogService> serviceMap=new HashMap<>();

    static {
        serviceMap.put(LogTypeEnum.EMAIL,SpringContextHolder.getBean("emailLogService"));
        serviceMap.put(LogTypeEnum.NOTE,SpringContextHolder.getBean("noteLogService"));
        serviceMap.put(LogTypeEnum.OTHER,SpringContextHolder.getBean("otherLogService"));
    }

    public enum LogTypeEnum{
        EMAIL,
        NOTE,
        OTHER;
    }


    public static AbstractLogService newLog(LogTypeEnum type) {

        return serviceMap.get(type);
    }
}
