/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.message.service.push.impl;

import com.je.common.base.DynaBean;
import com.je.common.base.mapper.query.NativeQuery;
import com.je.common.base.util.StringUtil;
import com.je.message.vo.Message;
import com.je.message.service.note.NoteServiceFactory;
import com.je.message.service.push.AbstractPlatformPushService;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.regex.Pattern;

/**
 * 短信
 *
 * @author wangmm@ketr.com.cn
 * @date 2020/5/09
 */
@Service("platformPushSms")
public class PlatformNotePushServiceImpl extends AbstractPlatformPushService {

    /**
     * ^ 匹配输入字符串开始的位置
     * \d 匹配一个或多个数字，其中 \ 要转义，所以是 \\d
     * $ 匹配输入字符串结尾的位置
     */
    private static final Pattern HK_PATTERN = Pattern.compile("^(5|6|8|9)\\d{7}$");
    private static final Pattern CHINA_PATTERN = Pattern.compile("^((13[0-9])|(14[0,1,4-9])|(15[0-3,5-9])|(16[2,5,6,7])|(17[0-8])|(18[0-9])|(19[0-3,5-9]))\\d{8}$");

    @Override
    public boolean send(Message message) {
        try {
            //变量替换
            format(message);
            //获取用户
            List<DynaBean> users = metaRbacService.selectByTableCodeAndNativeQuery("JE_RBAC_USER", NativeQuery.build().eq("JE_RBAC_USER_ID", message.getUserId()));

            //发送消息
            for (DynaBean user : users) {
                String phoneNumber = user.getStr("USER_PHONE");
                if (StringUtils.isNotBlank(phoneNumber)) {
                    String content = StringUtil.isNotEmpty(message.getTitle()) ? String.format("%s - %s", message.getTitle(), message.getContent()) : message.getContent();
                    NoteServiceFactory.getNoteService().sendNote(phoneNumber, content);
                }
            }
            return true;
        } catch (Exception e) {
            logger.error(" 短信 消息发送失败！[{}]{{}}", message.getTitle()
                    , message.getContent().length() > 100 ? message.getContent().substring(0, 100) : message.getContent());
            e.printStackTrace();
            return false;
        }
    }
}