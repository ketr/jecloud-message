package com.je.message.vo.weWork;

import java.util.List;

public class AbstractWeWorkNotice {
    /**
     * 用户id，jecloud用户  USER_ASSOCIATION_ID字段值，不是企业微信用户
     */
    private List<String> touser;
    /**
     * 指定接收消息的部门，部门ID列表，多个接收者用‘|’分隔，最多支持100个。
     * 当touser为"@all"时忽略本参数  企业微信部门id
     */
    private String toparty;
    /**
     * 指定接收消息的标签，标签ID列表，多个接收者用‘|’分隔，最多支持100个。
     * 当touser为"@all"时忽略本参数   企业微信标签id
     */
    private String totag;
    /**
     * 应用id
     */
    private String agentid;

    public List<String> getTouser() {
        return touser;
    }

    public void setTouser(List<String> touser) {
        this.touser = touser;
    }

    public String getToparty() {
        return toparty;
    }

    public void setToparty(String toparty) {
        this.toparty = toparty;
    }

    public String getTotag() {
        return totag;
    }

    public void setTotag(String totag) {
        this.totag = totag;
    }

    public String getAgentid() {
        return agentid;
    }

    public void setAgentid(String agentid) {
        this.agentid = agentid;
    }
}
