package com.je.message.logback;

import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.AppenderBase;
import com.alibaba.fastjson2.JSONObject;
import com.google.common.base.Strings;
import com.je.common.base.message.vo.PushSystemMessage;
import com.je.common.base.spring.SpringContextHolder;
import com.je.common.base.util.SQLFormatterUtil;
import com.je.common.base.util.SecurityUserHolder;
import net.sf.jsqlparser.parser.CCJSqlParserUtil;
import net.sf.jsqlparser.statement.Statement;
import net.sf.jsqlparser.statement.select.PlainSelect;
import net.sf.jsqlparser.util.deparser.StatementDeParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class RealtimeLogAppender extends AppenderBase<ILoggingEvent> {

    private static final Logger logger = LoggerFactory.getLogger(RealtimeLogAppender.class);

    public static final LogControlVo LOG_CONTROL_VO = new LogControlVo();
    public static final List<String> LOG_APPEND_USERS = new ArrayList<>();

    private static final List<String> SQL_CLS_LIST = new ArrayList<>();
    private static final List<String> EXCLUDE_CLS_LIST = new ArrayList<>();

    public static final String TOPIC = "topic.im.msg";

    static {
        SQL_CLS_LIST.add("com.je.ibatis.extension.plugins.JeIbatisInterceptor");
        EXCLUDE_CLS_LIST.add("org.apache.servicecomb.metrics.core.publish.DefaultLogPublisher");
        EXCLUDE_CLS_LIST.add("org.apache.servicecomb.common.rest.locator.ServicePathManager");
    }

    @Override
    protected void append(ILoggingEvent iLoggingEvent) {
        if ("com.je.message.logback.RealtimeLogAppender".equals(iLoggingEvent.getLoggerName())
                || EXCLUDE_CLS_LIST.contains(iLoggingEvent.getLoggerName())) {
            return;
        }
        if (LOG_APPEND_USERS.isEmpty()) {
            return;
        }

        List<String> userIdList = filterAppendUsers(iLoggingEvent);
        SenderRunnable senderRunnable = new SenderRunnable(SecurityUserHolder.getCurrentAccountName(), iLoggingEvent, userIdList);
        this.context.getScheduledExecutorService().execute(senderRunnable);
    }

    public void send(String logUserName, ILoggingEvent event, List<String> userIds) {
        if (userIds == null || userIds.isEmpty()) {
            return;
        }
        RedisTemplate<String, Object> redisTemplate = SpringContextHolder.getBean("redisTemplate");
        Environment environment = SpringContextHolder.getBean(Environment.class);
        if (redisTemplate == null || environment == null) {
            return;
        }
        try {
            logger.info("send realtime log to user:{}", userIds);
            JSONObject contentObj = new JSONObject();
            contentObj.put("product", environment.getProperty("servicecomb.service.name"));
            String formatSql = event.getFormattedMessage();
            if(LOG_CONTROL_VO.isFormatSql() && SQL_CLS_LIST.contains(event.getLoggerName())){
                formatSql = SQLFormatterUtil.format(formatSql);
            }
            contentObj.put("message", "操作人:" + (Strings.isNullOrEmpty(logUserName) ? "系统" : logUserName) + "，操作来源：" + event.getLoggerName() + ",操作内容：" + formatSql);
            PushSystemMessage message = new PushSystemMessage("realtimelog", contentObj.toJSONString());
            message.setTargetUserIds(userIds);
            redisTemplate.convertAndSend(TOPIC, message);
            logger.info("send realtime log to user:{} success", userIds);
        } catch (Throwable e) {
            e.printStackTrace();
            logger.error("send realtime log to user:{} error", userIds, e);
        }

    }

    private static String formatSql(String sql) {
        StringBuilder formatted = new StringBuilder();
        String[] lines = sql.split(System.lineSeparator());

        int indentLevel = 0;
        for (String line : lines) {
            line = line.trim(); // 去掉前后的空白字符
            if (line.isEmpty()) {
                continue;
            }

            if (line.startsWith(")")) {
                indentLevel--;
            }

            for (int i = 0; i < indentLevel; i++) {
                formatted.append("\t"); // 使用制表符作为缩进
            }
            formatted.append(line).append("\n");

            if (line.endsWith("(")) {
                indentLevel++;
            }
        }

        return formatted.toString();
    }

    /**
     * 找到真实要推送的用户
     *
     * @return
     */
    private List<String> filterAppendUsers(ILoggingEvent event) {
        String userId = SecurityUserHolder.getCurrentAccountRealUserId();
        if(Strings.isNullOrEmpty(userId)){
            return null;
        }
        if (LOG_CONTROL_VO.isForAllUser()) {
            if (LOG_CONTROL_VO.isOnlySql()) {
                if (SQL_CLS_LIST.contains(event.getLoggerName())) {
                    return LOG_APPEND_USERS;
                }
            } else {
                return LOG_APPEND_USERS;
            }
        } else {
            if (LOG_CONTROL_VO.getSpectialUserList() != null && LOG_CONTROL_VO.getSpectialUserList().contains(userId)) {
                if (LOG_CONTROL_VO.isOnlySql()) {
                    if (SQL_CLS_LIST.contains(event.getLoggerName())) {
                        return LOG_APPEND_USERS;
                    }
                }else {
                    return LOG_APPEND_USERS;
                }
            }
        }
        return null;
    }

    public static class LogControlVo {
        private boolean onlySql;
        private boolean formatSql;
        private boolean forAllUser;
        private List<String> spectialUserList;

        public boolean isOnlySql() {
            return onlySql;
        }

        public void setOnlySql(boolean onlySql) {
            this.onlySql = onlySql;
        }

        public boolean isFormatSql() {
            return formatSql;
        }

        public void setFormatSql(boolean formatSql) {
            this.formatSql = formatSql;
        }

        public boolean isForAllUser() {
            return forAllUser;
        }

        public void setForAllUser(boolean forAllUser) {
            this.forAllUser = forAllUser;
        }

        public List<String> getSpectialUserList() {
            return spectialUserList;
        }

        public void setSpectialUserList(List<String> spectialUserList) {
            this.spectialUserList = spectialUserList;
        }
    }

    class SenderRunnable implements Runnable {
        final String logUserName;
        final ILoggingEvent e;
        final List<String> userIds;

        SenderRunnable(String logUserName, ILoggingEvent e, List<String> userIds) {
            this.logUserName = logUserName;
            this.e = e;
            this.userIds = userIds;
        }

        @Override
        public void run() {
            RealtimeLogAppender.this.send(logUserName, e, userIds);
        }

    }

}
