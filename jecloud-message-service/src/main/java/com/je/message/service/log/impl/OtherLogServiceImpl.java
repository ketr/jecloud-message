/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.message.service.log.impl;

import com.google.common.base.Strings;
import com.je.common.base.DynaBean;
import com.je.common.base.service.rpc.BeanService;
import com.je.common.base.util.DateUtils;
import com.je.message.vo.LogMsg;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 *
 * @ClassName
 * @Author wangchao
 * @Date 2022/4/27 0027 10:27
 * @Version V1.0
 */
@Service("otherLogService")
public class OtherLogServiceImpl extends AbstractLogServiceImpl{

    @Override
    public void saveLog(LogMsg message) {
        //记录第三方消息推送日志
        DynaBean noteLog = new DynaBean("JE_MESSAGE_MEGLOG", false);
        noteLog.set(BeanService.KEY_PK_CODE, "JE_MESSAGE_MEGLOG_ID");
        noteLog.set("MEGLOG_FROMUSER", message.getFromUser());
        noteLog.set("MEGLOG_FROMUSERID", message.getFromUserId());
        noteLog.set("MEGLOG_TOUSER", message.getToUser());
        noteLog.set("MEGLOG_TOUSERID", message.getToUserId());
        noteLog.set("MEGLOG_SENDTIME", DateUtils.formatDateTime(new Date()));
        noteLog.set("MEGLOG_CONTEXT", message.getContext());
        noteLog.set("MEGLOG_FL", message.getFl());
        noteLog.set("MEGLOG_TITLE", message.getTitle());
        noteLog.set("MEGLOG_ADDRESSES", message.getFileKey());
        noteLog.set("MEGLOG_URL", message.getUrl());
        noteLog.set("MEGLOG_DEPTIDS", message.getDeptIds());
        noteLog.set("MEGLOG_BUTTON", message.getButtonText());
        if (Strings.isNullOrEmpty(message.getErrorMessage())) {
            noteLog.set("MEGLOG_STATUS", "1");
            noteLog.set("MEGLOG_RESULT", message.getResult());
        } else {
            noteLog.set("MEGLOG_STATUS", "0");
            noteLog.set("MEGLOG_FAILUREINFO", message.getErrorMessage());
        }
        commonService.buildModelCreateInfo(noteLog);
        metaService.insert(noteLog);
    }
}
