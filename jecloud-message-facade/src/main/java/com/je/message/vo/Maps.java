/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.message.vo;

import java.util.HashMap;
import java.util.Map;
/**
 * 信息工具类
 * @date 2018-05-22
 */
public class Maps {
	/**
	 * 成功状态值
	 */
	public static final String SUCCESS_VALUE = "1";
	/**
	 * 失败状态值
	 */
	public static final String ERROR_VALUE = "0";
	/**
	 * 信息key
	 */
	public static final String MESSAGE_KEY = "message";
	/**
	 * 状态key
	 */
	public static final String STATE_KEY = "status";
	/**
	 * 数据key
	 */
	public static final String DATA_KEY = "data";

	private Maps(){}
	/**
	 * 获取信息MapMessage实例
	 * @return
	 */

	public static MapMessageBulider MapMessageBulider(){
		return new MapMessageBulider();
	}
	/**
	 * 内部类：信息
	 * @author csy
	 * @date 2018-05-22
	 */
	public static class MapMessageBulider{
		
		private Map<String, Object> innerMap = new HashMap<String, Object>();
		/**
		 * 自由封装key/value
		 * @param key
		 * @param value
		 * @return
		 */
		public MapMessageBulider setKeyValue(String key,Object value){
			this.innerMap.put(key, value);
			return this;
		}
		/**
		 * 返回map
		 * @return
		 */
		public Map<String, Object> bulid(){
			return this.innerMap;
		}
		/**
		 * 设置成功返回值及信息
		 * 已设置默认的success/message/data key
		 * @param data 数据
		 * @param message 成功信息
		 * @return
		 */
		public Map<String, Object> setSuccessBulid(Object data,Object message){
			this.setKeyValue(STATE_KEY, SUCCESS_VALUE)
			.setKeyValue(MESSAGE_KEY, message)
			.setKeyValue(DATA_KEY, data);
			return this.bulid();
		}
		/**
		 * 设置返回失败数据及信息
		 * 已设置默认的success/message/data key
		 * @param data 数据
		 * @param message 失败信息
		 * @return
		 */
		public Map<String, Object> setErrorBulid(Object data,Object message){
			this.setKeyValue(STATE_KEY, ERROR_VALUE)
			.setKeyValue(MESSAGE_KEY, message)
			.setKeyValue(DATA_KEY, data);
			return this.bulid();			
		}
	}
}