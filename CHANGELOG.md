## 3.0.6 (2024-12-24)
### Features
- feat  : 解决sql注入问题

## 3.0.5 (2024-10-30)
### Features
- -feature:增加druid数据源监控配置

## 3.0.4 (2024-09-19)
### Features
- -feature:增加实时日志输出

## 3.0.3 (2024-07-26)
### Features
- feat (异常处理) : 移除MessageSendApiExceptionConverter
- feat (邮件发送) : 添加邮件发送接口

## 3.0.2 (2024-06-25)
### Features
- -feature:增加实时日志输出
- -feature:增加证书控制，调整版本至3.0.0

## 3.0.1 (2024-06-01)
### Features
- feat (短信发送) : 添加短信模版发送接口
- feat (dingTalk) : 添加钉钉消息推送

## 3.0.0 (2024-04-25)
### Features
- Revert "-feature:发布v3.0.0"
- -feature:发布v3.0.0
- feat (weWork) : 添加企业微信消息推送
- -feature:增加证书控制，调整版本至3.0.0

## 2.2.3 (2024-02-23)
### Features
- -feature:抽离nacos
- -feature:适配kingbase
- feat (dingTalk) : 添加钉钉消息推送
- -feature:适配nacos
