/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.message;

/**
 * Push
 *
 * @author wangmm@ketr.com.cn
 * @date 2020/5/25
 */
public enum Push {

    /**
     * web推送
     */
    WEB("WEB", "platformPushDWR"),

    /**
     * 短信
     */
    NOTE("NOTE", "platformPushSms"),

    /**
     * 邮件
     */
    EMAIL("EMAIL", "platformPushEmail"),


    /**
     * 钉钉
     */
    DINGTALK("DINGTALK", "platformPushDingTalk"),

    /**
     * 飞书
     */
    LARK("FLYBOOK", "platformPushLark"),

    /**
     * 企业微信
     */
    WECHAT("WECHAT", "platformPushWeChat"),

    /**
     * 华为WeLink
     */
    WELINK("WELINK", "platformPushWeLink"),
    ;

    /**
     * 平台对应字典[JE_CORE_PUSH_PLATFORM]编码
     */
    private String code;

    /**
     * 平台对应IOC容器实例名称
     */
    private String beanName;

    Push(String code, String beanName) {
        this.code = code;
        this.beanName = beanName;
    }

    /**
     * 检测
     *
     * @param code 编码
     * @return com.je.push.Push
     */
    public static Push check(String code) {

        //校验是否存在
        for (Push value : Push.values()) {
            if (value.getCode().equals(code)) {
                return value;
            }
        }
        return null;
    }

    public String getCode() {
        return code;
    }

    public String getBeanName() {
        return beanName;
    }
}
