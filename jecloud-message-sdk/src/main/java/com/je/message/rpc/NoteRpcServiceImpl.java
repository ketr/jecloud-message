/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.message.rpc;

import com.je.message.exception.MessageSendException;
import com.je.message.vo.NoteMsg;
import org.apache.servicecomb.provider.pojo.RpcReference;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class NoteRpcServiceImpl implements NoteRpcService {

    @RpcReference(microserviceName = "message", schemaId = "noteRpcService")
    private NoteRpcService noteRpcService;

    @Override
    public boolean sendNoteMsg(NoteMsg msgVo) {
        return noteRpcService.sendNoteMsg(msgVo);
    }

    @Override
    public int sendNoteCode(NoteMsg msgVo) {
        return noteRpcService.sendNoteCode(msgVo);
    }

    @Override
    public int sendWithRecordSendCode(String phoneNumber, String fromUser, String fromUserId, String toUser, String toUserId, String context) {
        return noteRpcService.sendWithRecordSendCode(phoneNumber, fromUser, fromUserId, toUser, toUserId, context);
    }


    @Override
    public boolean sendWithRecordSendUser(String phoneNumber, String fromUser, String fromUserId, String toUser, String toUserId, String context) throws MessageSendException {
        return noteRpcService.sendWithRecordSendUser(phoneNumber, fromUser, fromUserId, toUser, toUserId, context);
    }

    @Override
    public boolean sendWithRecordSendUserByTemplateCode(String phoneNumber, String fromUser, String fromUserId, String toUser, String toUserId, String context, String serviceType, String signName, String templateCode, Map<String, String> params) {
        return noteRpcService.sendWithRecordSendUserByTemplateCode(phoneNumber, fromUser, fromUserId, toUser, toUserId, context, serviceType, signName, templateCode, params);
    }


    @Override
    public boolean sendSimple(String phoneNumber, String context) throws MessageSendException {
        return noteRpcService.sendSimple(phoneNumber, context);
    }

    @Override
    public boolean sendTemplate(String phoneNumber, String tempCode, Map<String, Object> params) throws MessageSendException {
        return noteRpcService.sendTemplate(phoneNumber, tempCode, params);
    }


    @Override
    public int sendNote(String phoneNumber, String context) {
        return noteRpcService.sendNote(phoneNumber, context);
    }

    @Override
    public boolean sendNoteByTemplateCode(String number, String context, String signName, String templateCode, Map<String, String> params) {
        return noteRpcService.sendNoteByTemplateCode(number, context, signName, templateCode, params);
    }

}
