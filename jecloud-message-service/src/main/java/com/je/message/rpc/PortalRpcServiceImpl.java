/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.message.rpc;

import com.je.common.base.message.vo.NoticeMsg;
import com.je.common.base.result.BaseRespResult;
import com.je.message.service.portal.PortalService;
import com.je.message.vo.WebPushTypeEnum;
import org.apache.servicecomb.provider.pojo.RpcSchema;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @ClassName
 * @Author wangchao
 * @Date 2022/6/22 0022 13:51
 * @Version V1.0
 */
@RpcSchema(schemaId = "portalRpcService")
public class PortalRpcServiceImpl implements PortalRpcService{

    @Autowired
    private PortalService portalService;

    @Override
    public BaseRespResult insertOrUpdateSign(String userId, String deptId, WebPushTypeEnum pushTypes, String actionType) {
        return portalService.insertOrUpdateSign(userId, deptId, pushTypes, actionType);
    }


    @Override
    public BaseRespResult insertNoticeMsg(NoticeMsg noticeMsg) {
        return portalService.insertNoticeMsg(noticeMsg);
    }
}
